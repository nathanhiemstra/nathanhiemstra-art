<?
	// variables
	$title = "Paralell Worlds in Reflections";
	$section_name = "photographs";
	$project_name = "parallel-worlds-in-reflections";
	$page_name = "parallel-worlds-in-reflections";
	$description = "Parallel worlds I see in relections.";

	include "../inc/header-html.php";
	include "../_lib/functions/build-thumbnails.php"; 
?>

</head>
<body class="p-<?=$page_name?>">
	<? include "../inc/header.php"; ?>






	<div class="row">
		<header role="banner">
			<div class="container hd clearfix">
				<h1><?=$title?></h1>	
			</div><!-- end .container -->

		</header><!-- end #hd -->
	</div>
	<div class="container hd" class="main">
		<section>			
			<div class="row">
				<ul class="thumbnails-default">
					<?php
						buildThumbnails($project_name);
					?>
				</ul>
			</div>
		</section>
	</div><!-- /. main -->
<?php include "../inc/footer.php"; ?>
