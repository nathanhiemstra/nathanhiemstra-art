<?
	// variables
	$title = "Quote Wheels - Mark";
	$section_name = "social-practice";
	$project_name = "quote-wheels";
	$page_name = "quote-wheels-mark";
	$description = "";

	include "../inc/header-html.php";
	include "../_lib/functions/build-thumbnails.php"; 
?>
<style type="text/css">
/**/
</style>
</head>
<body class="p-<?=$page_name?> s-quote-wheels">

	<? include "../inc/header.php"; ?>
	<div class="row">
		<header role="banner">
			<div class="container hd clearfix">
				<h1><?=$title?></h1>	
			</div><!-- end .container -->

		</header><!-- end #hd -->
	</div>
	<div class="container hd" class="main">
		<section>			
			<div class="row">
				<div class="columns eight">
					<div class="cutout-container">
						<img src="../_lib/images/quote-wheels/lg/mark.jpg" alt="Quote Wheel - Mark" class="full-width">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="columns five offset-one">
					<ul class="quote-list">
						<li>
							<span class="qw-date">3/4/2014</span>
							There’s no defense and it’s still a hard game.
							<span class="context">– About bowling</span>   
						</li>
						<li>
							<span class="qw-date">1/17/2014</span>
							I like thinking of Marina as an alcoholic bitch.
						</li>
						<li>
							<span class="qw-date">3/11/2014</span>
							The left Jeff.
							<span class="context">- Identifying which of the multiple Jeffs he meant</span>
						</li>
						<li>
							<span class="qw-date">5/23/2014</span>
							I love accidental genius.
							<span class="context">– After declaring his memoir would be titled “It's Read for a Reason”</span>
						</li>
						<li>
							<span class="qw-date">6/5/2014</span>
							Life is cool, huh?
							<span class="context">– At the end of a slap-happy work meeting</span>
						</li>
						<li>
							<span class="qw-date">6/25/2014</span>
							Or, the opposite of a rabbit hole. A scattered mess.
						</li>
						<li>
							<span class="qw-date">6/25/2014</span>
							I thought that one told itself.
							<span class="context">– After simply saying the punchline “What’s a motto?”</span>
						</li>
						<li>
							<span class="qw-date">6/25/2014</span>
							They have to ask us the questions, we can't just answer them. 
							<span class="context">- About our work client</span>
						</li>
						<li>
							<span class="qw-date">6/25/2014</span>
							It’s like having an imaginary friend, but real.
						</li>
						<li>
							<span class="qw-date">7/8/2014</span>
							I'm kind of all in.
						</li>
						<li>
							<span class="qw-date">7/8/2014</span>
							“Accidentally Racist.” That should be our band name!
						</li>
						<li>
							<span class="qw-date">7/8/2014</span>
							Soft on-board.
							<span class="context">- About our work project</span>
						</li>
						<li>
							<span class="qw-date">7/15/2014</span>
							I need dumb puns.
							<span class="context">- Explaining why he didn’t get my joke involving the name Andrew Lloyd Webber.</span>
						</li>
						<li>
							<span class="qw-date">7/17/2014</span>
							I know they’re not going to want open a fly-out just to open a drop-down. 
							<span class="context">- About our work client</span>
						</li>
						<li>
							<span class="qw-date">7/17/2014</span>
							I prefer the one with the dead head tab rather than the dead head indicator.
							<span class="context">- About our work project</span>
						</li>
						<li>
							<span class="qw-date">8/8/2014</span>
							If we can keep Steven Hawking alive this long, we should be able to keep George R. R. Martin alive long enough to finish the books. 
						</li>
						<li>
							<span class="qw-date">8/15/2014</span>
							Jealousy is a weird feeling.
						</li>
						<li>
							<span class="qw-date">2/2014/2015</span>
							I just want to do good work and not get sad
							<span class="context">- About work priorities</span>
						</li>
						<li>
							<span class="qw-date">2/2014/2015</span>
							I feel perfect 
						</li>
						<li>
							<span class="qw-date">2/2014/2015</span>
							She did those pink lawyer movies.
							<span class="context">- About Reese Witherspoon </span>
						</li>
						<li>
							<span class="qw-date">7/24/2015</span>
							Do you ever waste your time on purpose? 
						</li>
						<li>
							<span class="qw-date">7/24/2015</span>
							I hadn't had sex with this girl yet, and spoiler alert, I never have.
						</li>
						<li>
							<span class="qw-date">7/24/2015</span>
							"Gum." Every word is weird. 
						</li>
						<li>
							<span class="qw-date">9/2/2015</span>
							We're gonna go nuts, maybe. 
						</li>
						<li>
							<span class="qw-date">9/18/2015</span>
							I used to steal the young dudes to be my friends. 
						</li>
						<li>
							<span class="qw-date">9/23/2015</span>
							I hate unique. (You know what I mean)
						</li>
						<li>
							<span class="qw-date">7/26/16</span>
							This blanket is round. 
							<span class="context">- About his sleeping bag</span>
						</li>
					</ul>
				</div>
			</div>
		</section>
	</div><!-- /. main -->
<?php include "../inc/footer.php"; ?>