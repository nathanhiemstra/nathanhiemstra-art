<?
	// variables
	$title = "Movie Spoiler Posters";
	$section_name = "collections";
	$project_name = "movie-ruining-posters";
	$page_name = "movie-ruining-posters";
	$description = "Spoilers!";

	include "../inc/header-html.php";

	mysql_select_db($database_nh_com_db, $nh_com_db);
	$query_projects_home = "SELECT *,category.title AS category_title, project.title AS project_title, project.path AS project_path, category.path AS category_path FROM project LEFT JOIN category ON category.id=project.category_id WHERE project.active = 'true' AND category.active = 'true' ORDER BY category.`order`, project.`order` ASC";
	$projects_home = mysql_query($query_projects_home, $nh_com_db) or die(mysql_error());
	$row_projects_home = mysql_fetch_assoc($projects_home);
	$totalRows_projects_home = mysql_num_rows($projects_home);

	include "../_lib/functions/build-thumbnails.php"; 
?>

</head>
<body class="p-<?=$page_name?>">
	<? include "../inc/header.php"; ?>
	<div class="row">
		<header role="banner">
			<div class="container hd clearfix">
				<h1><?=$title?></h1>	
			</div><!-- end .container -->

		</header><!-- end #hd -->
	</div>
	<div class="container hd" class="main">
		<section>			
			<div class="row">
				<ul class="thumbnails-default">
					<?php
						buildThumbnails($project_name);
					?>
				</ul>
			</div>
		</section>
	</div><!-- /. main -->
<?php include "../inc/footer.php"; ?>
