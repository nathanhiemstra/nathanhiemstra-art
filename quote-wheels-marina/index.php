<?
	// variables
	$title = "Quote Wheels - Marina";
	$section_name = "social-practice";
	$project_name = "quote-wheels";
	$page_name = "quote-wheels-marina";
	$description = "";

	include "../inc/header-html.php";
	include "../_lib/functions/build-thumbnails.php"; 
?>
<style type="text/css">
/**/
</style>
</head>
<body class="p-<?=$page_name?> s-quote-wheels">

	<? include "../inc/header.php"; ?>
	<div class="row">
		<header role="banner">
			<div class="container hd clearfix">
				<h1><?=$title?></h1>	
			</div><!-- end .container -->

		</header><!-- end #hd -->
	</div>
	<div class="container hd" class="main">
		<section>			
			<div class="row">
				<div class="columns eight">
					<div class="cutout-container">
						<img src="../_lib/images/quote-wheels/lg/marina.jpg" alt="Quote Wheel - Marina" class="full-width">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="columns five offset-one">
					<ul class="quote-list">
						<li>
							<span class="qw-date">7/16/2013</span>
							I'll draw you a sandwich and then you can eat this piece of paper.
						</li>
						<li>
							<span class="qw-date">7/21/2013</span>
							It was such a long week yesterday.
						</li>
						<li>
							<span class="qw-date">7/23/2013</span>
							Now I know how president Obama feels. 
							<span class="context">- About the stress and flak that comes with being Pizza Club president at work, especially the rescheduling</span>
						</li>
						<li>
							<span class="qw-date">2/28/2014</span>
							I don’t have anything gold except for my last name. (Goldshteyn)
						</li>
						<li>
							<span class="qw-date">2/28/2014</span>
							I have a really bad skiing story. I crashed and I woke up with the biggest wedgie.
						</li>
						<li>
							<span class="qw-date">6/24/2015</span>
							Thanks. I didn't know it would be so appropriate. 
							<span class="context">- After my compliment of her dress at a wedding</span> 
						</li>
						<li>
							<span class="qw-date">9/19/2015</span>
							There're some words I don't like and some words I really really like. 
						</li>
						<li>
							<span class="qw-date">9/24/2015</span>
							Ahhhh. I should have had more candy today. 
							<span class="context">- This being this the very first thing she said after greetng me when I arrived at her house.</span>
						</li>
						<li>
							<span class="qw-date">1/25/2016</span>
							Some people aren't funny and I pretend those people don't exist.
						</li>
					</ul>
				</div>
			</div>
		</section>
	</div><!-- /. main -->
<?php include "../inc/footer.php"; ?>