<?
	// variables
	$title = "Circus Wagon Wheels";
	$section_name = "photography";
	$project_name = "circus-wagon-wheels";
	$page_name = "circus-wagon-wheels";
	$description = "Circus Wagon Wheels";

	include "../inc/header-html.php";
	include "../_lib/functions/build-thumbnails.php"; 
?>

</head>
<body class="p-<?=$page_name?>">
	<? include "../inc/header.php"; ?>
	<div class="row">
		<header role="banner">
			<div class="container hd clearfix">
				<h1><?=$title?></h1>
				<div class="project-description">
					<p>I visited <a href="https://www.circusworldbaraboo.org/" target="_blank">Circus World Museum</a> and was taken by the design and painted patterns on circus wagon wheels. I love seeing something so functional be so ornate.</p>	
				</div>
				
			</div><!-- end .container -->

		</header><!-- end #hd -->
	</div>
	<div class="container hd" class="main">
		<section>			
			<div class="row">
				<ul class="thumbnails-default">
					<?php
						buildThumbnails($project_name);
					?>
				</ul>
			</div>
		</section>
	</div><!-- /. main -->
<? include "../inc/footer.php"; ?>
