<?
	// variables
	$title = "Band and the Names";
	$section_name = "creativity-exercises";
	$project_name = "band-and-the-names";
	$page_name = "band-and-the-names	";
	$description = "";

	include "../inc/header-html.php";
	include "../_lib/functions/build-thumbnails.php"; 
?>
<style type="text/css">
/**/
</style>
</head>
<body class="p-<?=$page_name?>">

	<? include "../inc/header.php"; ?>
	<div class="row">
		<header role="banner">
			<div class="container hd clearfix">
				<h1><?=$title?></h1>	
				<p><a href="https://twitter.com/bandandthenames" target="_blank">twitter.com/bandandthenames</a></p>
			</div><!-- end .container -->

		</header><!-- end #hd -->
	</div>
	<div class="container hd" class="main">
		<section>			
			<div class="row">
				<div class="columns six">
					<ul class="label-list">
						<li>Pangea and the Continental Drifters</li>
					<li>Dancer and the Prancers</li>
						<li>Woodchuck and the Could Chucks</li>
						<li>Paragraph and the Hard Returns </li>
						<li>Lamp Shade and the Diffusers</li>
						<li>Patti-Whack and the Dog Bones</li>
						<li>Bacon and the Sizzlers</li>
						<li>King Solomon and the Solomon Grundy's</li>
						<li>Pink Eye and the Crusters</li>
						<li>Swimming and the Weightless Feelings</li>
						<li>Lemon and the Limers</li>
						<li>Garbage And Trashers</li>
						<li>Cloud and the Evolving Shapes </li>
						<li>Roman Candle and the Summer Sandals</li>
						<li>Bedtime Banana and the Slippers</li>
						<li>Temper and the Shorties</li>
						<li>Shampoo and the Rinsers </li>
						<li>Dog and the People Pleasers</li>
						<li>Foot Rub and the True Signs of Love</li>
						
					</ul>
				</div>
				<div class="columns six">
					<ul class="label-list">
						<li>Lever and the Simple Machines</li>
						<li>Justin Case and the What Ifs</li>
						<li>Nixon and the Helicopter Exits</li>
						<li>Tornado and the Aftermaths</li>
						<li>ADHD and the Spans of Attention</li>
						<li>Dummy and the Smarty Pants</li>
						<li>Bowel and the Grunter</li>
						<li>Necro and the Feelers</li>
						<li>God and the Resters</li>
						<li>Over and the Seers</li>
						<li>Softy and the Noodlers</li>
						<li>Cat and the Cradlers</li>
						<li>Cause and the Effecters</li>
						<li>Deli and the Caterers</li>
						<li>Tinder and the Sticklers</li>
						<li>Grandma and the Spoilers </li>
						<li>Toothpaste and the Squeezers</li>
						<li>High Noon and the Showdowns </li>
						<li>Mumbo and the Jumblers</li>
					</ul>
				</div>
			</div>
		</section>
	</div><!-- /. main -->
<?php include "../inc/footer.php"; ?>