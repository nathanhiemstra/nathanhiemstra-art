<?
	// variables
	$title = "Talking With Kids";
	$section_name = "audio";
	$project_name = "talking-with-kids";
	$page_name = "talking-with-kids";
	$description = "I love improvising with kids.";
	$initialize_js_funtion = "audioSoundboardLong()";

	include "../inc/header-html.php";
?>
</head>
<body class="p-<?=$project_name?> audio-soundboard-long">
	<? include "../inc/header.php"; ?>
	<div class="row p-<?=$project_name?>">
		<header role="banner">
			<div class="container hd clearfix">
				<div class="columns nine">
					<h1>Talking With Kids</h1>
				</div>
			</div><!-- end .container -->

		</header><!-- end #hd -->
	</div>

	<div class="container hd" id="main">
		<div class="row">
			<div class="columns twelve audio-container">
				<?
					$json_file = file_Get_contents("../_lib/json/".$project_name."/audio-files.json");
					$json = json_decode($json_file);
					$total_entries = 0;
					foreach($json->entries as $record){
						$total_entries++;
					}
					$entries_per_column = round($total_entries / 2);
				?>
				<div class="columns six">
					<ul>
						<?
							// Make columns
							foreach($json->entries as $record){
								if ($i == $entries_per_column) {
									print '</ul></div><div class="columns six"><ul >';
									$i = 0;
								}
								$i++;
						?>
						<li class="item">
							<div class="clearfix">
								<span class="date">
									<span><?=date("l, ",strtotime($record->date))?></span>
									<?=date("F j, Y",strtotime($record->date))?>
								</span>
								<h3 class="title"><?=$record->title?></h3>
								<? /*<span class="duration"><?=$record->duration;?></span> */ ?>
							</div>

							<audio controls preload="metadata" title="<?=$record->title?> <span><?=$record->duration;?></span>">
								<source src="/_lib/audio/<?=$project_name?>/mp3/<?=$record->filename?>.mp3" type="audio/mpeg">
								<source src="/_lib/audio/<?=$project_name?>/ogg/<?=$record->filename?>.ogg" type="audio/ogg">
							</audio>
							<? if ($record->lyric) { ?>
								<div class="transcript" >
									<? if (strlen($record->lyric) > 500) { ?>
									<p class="transcript-trigger"><a href="#transcript-<?=$i?>">Hide</a></p>
									<? }  ?>
									<p class="transcript-content" id="transcript-<?=$i?>"><?=$record->lyric?></p>
									<p class="transcript-trigger"><a href="#transcript-<?=$i?>">Hide</a></p>
								</div>
							<? } ?>
						</li>
						<? } ?>
					</ul>
				</div>
			</div>
		</div>

		<div class="row ">
			<p>
				<a href="#" class="transcripts-all-show">Show  transcipts</a>
				<a href="#" class="transcripts-all-hide">Hide  transcipts</a>
			</p>
		</div>

<? /*
		<div class="columns twelve">

			<div id="disqus_thread"></div>
		    <script type="text/javascript">
		        // CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE
		        var disqus_shortname = 'mikebike'; // required: replace example with your forum shortname

		        // DON'T EDIT BELOW THIS LINE
		        // (function() {
		        //     var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
		        //     dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
		        //     (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
		        // })();
		    </script>
		    <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>

		</div>
*/?>
	</div><!-- end #main -->



<?php include "../inc/footer.php"; ?>
