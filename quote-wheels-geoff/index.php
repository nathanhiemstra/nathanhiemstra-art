<?
	// variables
	$title = "Quote Wheels - Geoff";
	$section_name = "social-practice";
	$project_name = "quote-wheels";
	$page_name = "quote-wheels-geoff";
	$description = "";

	include "../inc/header-html.php";
	include "../_lib/functions/build-thumbnails.php"; 
?>
<style type="text/css">
/**/
</style>
</head>
<body class="p-<?=$page_name?> s-quote-wheels">

	<? include "../inc/header.php"; ?>
	<div class="row">
		<header role="banner">
			<div class="container hd clearfix">
				<h1><?=$title?></h1>	
			</div><!-- end .container -->

		</header><!-- end #hd -->
	</div>
	<div class="container hd" class="main">
		<section>			
			<div class="row">
				<div class="columns eight">
					<div class="cutout-container">
						<img src="../_lib/images/quote-wheels/lg/geoff.jpg" alt="Quote Wheel - Geoff" class="full-width">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="columns five offset-one">
					<ul class="quote-list">
						<li>
							<span class="qw-date">8-28-1995</span>
							Let me kiss upon you with my, OWN lips.
							<span class="context">- About Darth Vader's girlfriend</span>
						</li>
						<li>
							<span class="qw-date">8-30-1995</span>
							I don't want to live near barbed wire.
						</li>
						<li>
							<span class="qw-date">11-22-1995</span>
							If I was him, I’d just jump off. He's gonna live.
							<span class="context">- About Willie E. Coyote</span>
						</li>
						<li>
							<span class="qw-date">12-1-1995</span>
							Remember: If you die with amnesia, you won't remember dying.
						</li>
						<li>
							<span class="qw-date">12-21-1995</span>
							He's the king of making anything rhyme with like... anything. 
						</li>
						<li>
							<span class="qw-date">1-27-1996</span>
							I don't want to see any more commercials with old people doing young people things. But I do want to see more commercials with animals talking.
						</li>
						<li>
							<span class="qw-date">5-4-1996</span>
							Knives make any trick a little bit better.
						</li>
						<li>
							<span class="qw-date">9-3-1996</span>
							That's the difference between me and you. You like sweet and I like coffee.
						</li>
						<li>
							<span class="qw-date">9-6-1996</span>
							Even though that show was based on a candy, it was funny as hell. 
							<span class="context">- About Gummy Bears</span>
						</li>
						<li>
							<span class="qw-date">10-1-1996</span>
							I did think it was pretty similar when Mork opened his hand and said “nanu nanu”, and spock opened his hand and said... what he said.
						</li>
						<li>
							<span class="qw-date">10-2-1996</span>
							I justify it because it’s me.
						</li>
						<li>
							<span class="qw-date">10-27-1996</span>
							I don't know how tall he is, I'm drunk. 
						</li>
						<li>
							<span class="qw-date">11-22-1996</span>
							Leave it to Marlboro to put the Grand Canyon under water.
							<span class="context">- About a billboard</span>
						</li>
						<li>
							<span class="qw-date">12-5-1996</span>
							That guy doesn't have teeth but he's got a puppy. That's not where my priorities would be.
						</li>
						<li>
							<span class="qw-date">12-9-1996</span>
							Oh my God, who's gonna play on the opening night of the moon? Who ever it is, they're gonna blow up on the way there.
							<span class="context">- About future moon colonization</span>
						</li>
						<li>
							<span class="qw-date">12-31-1996</span>
							It's not that I was stupid in 7th grade, it's just that I was into discovering things by physically attacking them.
						</li>
						<li>
							<span class="qw-date">1-25-1998</span>
							Man, can't somebody just not use the computer to make a commercial.
							<span class="context">- Watching Super Bowl commercials</span>
						</li>
						<li>
							<span class="qw-date">2-5-1997</span>
							I wish there were special bullets that shot into the TV. It'd be like cigarettes, I’d have to have them.
						</li>
						<li>
							<span class="qw-date">5-22-1997</span>
							Eww, there's another mother figure in my life.
						</li>
						<li>
							<span class="qw-date">7-4-1997</span>
							I bet cavemen would love fireworks.
						</li>
						<li>
							<span class="qw-date">7-18-1998</span>
							You're eating cereal while I'm drinking beer.
							<span class="context">- To our friend Terry, around noon.</span>
						</li>
						<li>
							<span class="qw-date">12-26-1998</span>
							It's a bad idea, but sometimes I like bad ideas.
						</li>
						<li>
							<span class="qw-date">8-13-2000</span>
							This is awesome. They'd appreciate it if they went to art school.
							<span class="context">- In our pinhole photography class at camp.</span>
						</li>
						<li>
							<span class="qw-date">7-19-2002</span>
							Remember what it felt like in eighth grade when you found out that the girl you really liked, liked you too? That's what ecstasy is like.
						</li>
						<li>
							<span class="qw-date">7-22-2002</span>
							You know every trucker must have like two or three stories of just blasting a deer.
						</li>
					</ul>
				</div>
			</div>
		</section>
	</div><!-- /. main -->
<?php include "../inc/footer.php"; ?>