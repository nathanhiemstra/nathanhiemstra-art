<?
	// variables
	$title = "Secret Hideawys";
	$section_name = "drawings";
	$project_name = "secret-hideaways";
	$page_name = "secret-hideaways";
	$description = "";

	include "../inc/header-html.php";
	include "../_lib/functions/build-thumbnails.php"; 
?>

</head>
<body class="p-<?=$page_name?>">
	<? include "../inc/header.php"; ?>
	<div class="row">
		<header role="banner">
			<div class="container hd clearfix">
				<h1><?=$title?></h1>
			</div><!-- end .container -->
		</header><!-- end #hd -->
	</div>
	<div class="row">
		<div class="container hd">
			<div class="columns ten">
				<p>These are drawings of places that my inner child would like to go. In fact, the drawing process is a dialog with my inner child. I draw an item, then I look at an adjacent empty space and ask my inner child “what would be comforting?” or “what would inspire wonder?” to see there. 
				<p>The Artist’s Way describes the inner child being the ego state that creativity runs through. In this way, these drawings also become a creativity exercise.</p>
			</div>
			<div class="columns two"></div>
		</div><!-- end .container -->
	</div>
	<div class="container hd" class="main">
		<section>			
			<div class="row">
				<ul class="thumbnails-default">
					<?php
						buildThumbnails($project_name);
					?>
				</ul>
			</div>
		</section>
	</div><!-- /. main -->
<?php include "../inc/footer.php"; ?>
