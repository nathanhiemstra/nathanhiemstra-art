<?
	// variables
	$title = "Eye Contact With Passing Cars";
	$section_name = "photographs";
	$project_name = "eye-contact-with-passing-cars";
	$page_name = "eye-contact-with-passing-cars";
	$description = "The strict unspoken rules eye contact seem to be lifted while we are safely in separate cars.";

	include "../inc/header-html.php";
	include "../_lib/functions/build-thumbnails.php"; 
?>

</head>
<body class="p-<?=$page_name?>">
	<? include "../inc/header.php"; ?>
	<div class="row">
		<header role="banner">
			<div class="container hd clearfix">
				<h1><?=$title?></h1>	
			</div><!-- end .container -->

		</header><!-- end #hd -->
	</div>
	<div class="container hd" class="main">
		<section>			
			<div class="row">
				<ul class="thumbnails-default">
					<?php
						buildThumbnails($project_name);
					?>
				</ul>
			</div>
		</section>
	</div><!-- /. main -->
<?php include "../inc/footer.php"; ?>
