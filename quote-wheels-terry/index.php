<?
	// variables
	$title = "Quote Wheels - Terry";
	$section_name = "social-practice";
	$project_name = "quote-wheels";
	$page_name = "quote-wheels-terry";
	$description = "";

	include "../inc/header-html.php";
	include "../_lib/functions/build-thumbnails.php"; 
?>
<style type="text/css">
/**/
</style>
</head>
<body class="p-<?=$page_name?> s-quote-wheels">

	<? include "../inc/header.php"; ?>
	<div class="row">
		<header role="banner">
			<div class="container hd clearfix">
				<h1><?=$title?></h1>	
			</div><!-- end .container -->

		</header><!-- end #hd -->
	</div>
	<div class="container hd" class="main">
		<section>			
			<div class="row">
				<div class="columns eight">
					<div class="cutout-container">
						<img src="../_lib/images/quote-wheels/lg/terry.jpg" alt="Quote Wheel - Terry" class="full-width">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="columns five offset-one">
					<ul class="quote-list">
						<li>
							<span class="qw-date">1/10/1998</span>
							Or, if twin dummies died, I could bury them in this.
							<span class="context">- About a two-compartment wooden box.</span>
						</li>
						<li>
							<span class="qw-date">2/2/1998</span>
							I know static means something. It has to.
						</li>
						<li>
							<span class="qw-date">2/10/1998</span>
							Is that a homeless cat, or a brown bag?
						</li>
						<li>
							<span class="qw-date">7/15/1998</span>
							You can have arms through positive thinking, just try!
							<span class="context">- About the idea of footstools playing volleyball.</span>
						</li>
						<li>
							<span class="qw-date">7/28/1998</span>
							I don’t care who you are, you can't order a hot chocolate in this place and sound threatening.
							<span class="context">- At a fancy cafe</span>
						</li>
						<li>
							<span class="qw-date">9/4/2000</span>
							Human beings just shouldn't wait tables.
						</li>
						<li>
							<span class="qw-date">7/29/2002</span>
							People have every right to be complicated.
						</li>
						<li>
							<span class="qw-date">7/27/2002</span>
							I don’t know what I want to do, I just stepped in gum.
						</li>
						<li>
							<span class="qw-date">10/26/2003</span>
							Yeah, she’ll probably be a serious baby until she loosens up.
							<span class="context">- About his ten-week-old</span>
						</li>
						<li>
							<span class="qw-date">9/1/2013</span>
							I can't believe that place is still standing. Most places I worked have gone under. 
							<span class="context">- About a pizza place where we worked together</span>
						</li>
						<li>
							<span class="qw-date">2/9/2014</span>
							Brown teeth don't  even look good in a brown statue. 
							<span class="context">- About the Bronze Fonze, in Milwaukee</span>
						</li>
						<li>
							<span class="qw-date">1/2/2015</span>
							Everyone sounds sexy and impatient. 
							<span class="context">- About watching TV in Spanish. </span>
						</li>
						<li>
							<span class="qw-date">1/1/2015</span>
							I feel like I'm walking into a clothes store in the mall with a bunch of attractive girls working there who accept me for who I am. 
							<span class="context">- About the Nintendo Wii background music</span>
						</li>
						<li>
							<span class="qw-date">1/1/2015</span>
							In seventh grade I had this vaguely Scandinavian character that I drew that served no purpose. His name was Heurgon Feurgon. 
						</li>
						<li>
							<span class="qw-date">9/4/2015</span>
							If I get sarcastic with him, I feel like I'm stabbing a baby. 
							<span class="context">- About a very sincere friend</span>
						</li>
						<li>
							<span class="qw-date">9/4/2015</span>
							He's not even 70 and he sounds 95 years old. 
						</li>
						<li>
							<span class="qw-date">1/4/2016</span>
							Mmm... tangible and exciting. So mutually exclusive. 
						</li>
						<li>
							<span class="qw-date">1/5/2016</span>
							It’s like if the Hulk were articulate.
							<span class="context">- About how the more angry Dera (his wife) gets, the more precisly she speaks.</span>
						</li>
					</ul>
				</div>
			</div>
		</section>
	</div><!-- /. main -->
<?php include "../inc/footer.php"; ?>