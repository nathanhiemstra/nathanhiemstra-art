<?
    function seoUrl($string) {
        //Lower case everything
        $string = strtolower($string);
        // //Make alphanumeric (removes all other characters)
        // $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
        // //Clean up multiple dashes or whitespaces
        $string = preg_replace("/[\s-]+/", " ", $string);
        // //Convert whitespaces and underscore to dash
        $string = preg_replace("/[\s_]/", "-", $string);
        return $string;
    }
    // No longer used! 9/22/2015
    function setUpAnimateFoundPhotos($title) {
        $project_name = "animate-found-photos";
        $page_name_partial = seoUrl($title);
        $title = $title."";
        $page_name = $project_name."-".$page_name_partial;
        // echo $page_name;
        // echo $duplicate_image;
        return array($project_name,$title,$page_name,$page_name_partial);
    }
?>