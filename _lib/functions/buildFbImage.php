<?
	if (!$thumb_size)  {
		$thumb_size = "sm";
	}
    function buildFbImage($project_name,$thumb_size) {
		$path_relative = "../_lib/images/".$project_name."/".$thumb_size;
		$path_absolute = "http://".$_SERVER['HTTP_HOST']."/_lib/images/".$project_name."/".$thumb_size."/";
	
		$cur = 1;
		// echo $project_name;
		// echo $thumb_size;
		// echo $path_relative;
		// echo $path_absolute;


		// Loop through thumbs
		if ($dir = @opendir($path_relative)) {
			while (($file = readdir($dir)) !== false) {
				switch($file) {
					case ".DS_Store":
					break;
					case ".":
					break;
					case "..":
					break;
					default: 
					if ($cur > 1) {
						break;
					} 
					$count_fb_image ++;
					$cur++;
					print '<meta property="og:image" content="'.$path_absolute.$file.'">';
					break;
				} // end switch
			}  // end while 
			closedir($dir);
		} // end if 
	}
?>