<?
function buildThumbnails($project_name) {
	// Variables
	$pathSM = "../_lib/images/".$project_name."/sm";
	$pathLG = "../_lib/images/".$project_name."/lg";
	$fancybox_effect = 'fancybox-effects-c';
	if ($project_name == 'circus-wagon-wheels')  {
		$fancybox_effect = 'fancybox-effects-c2';
	}
	$cur = 1;
	$count_fb_image = 1;
	$new_row = '</ul>';
	$new_row .= "\r\n\t\t\t\t\t";
	$new_row .= '</div>';
	$new_row .= "\r\n\t\t\t\t\t";
	$new_row .= '<div class="row">';
	$new_row .= "\r\n\t\t\t\t\t\t";
	$new_row .= '<ul class="thumbnails-default">';
	$new_row .= "\r\n\t\t\t\t\t\t\t";	

	$files = array();
	foreach (new DirectoryIterator($pathSM) as $fileInfo) {
	    if($fileInfo->isDot()) continue;
	    if ($fileInfo != '.' && $fileInfo != '..' && $fileInfo != '.DS_Store' && $fileInfo != 'Thumbs.db') {
		    $files[$fileInfo->getFilename()] = $fileInfo->getFilename();
		}
	}
	krsort($files);
	// echo '<pre>'; print_r($files); echo '</pre>';

	foreach ($files as $file) {
	    if ($cur == 7) {
			echo $new_row;
			$cur = 1;
		} 
		$count_fb_image ++;
		$cur++;
	    print ' 
		<li class="columns two thumb-wrapper">
			<a href="'.$pathLG.'/'.$file.'" class="'.$fancybox_effect.' thumbnail" data-fancybox-group="thumb" rel="fancybox-button" >
				<img src="'.$pathSM.'/'.$file.'" alt="'.$file.'"  /> 
			</a>
		</li>';
	} 
}


function buildThumbnailsLG($project_name) {
	// Variables
	$pathLG = "../_lib/images/".$project_name."/lg";
	$pathSM = $pathLG;
	$cur = 1;
	$count_fb_image = 1;
	$new_row = '</ul>';
	$new_row .= "\r\n\t\t\t\t\t";
	$new_row .= '</div>';
	$new_row .= "\r\n\t\t\t\t\t";
	$new_row .= '<div class="row">';
	$new_row .= "\r\n\t\t\t\t\t\t";
	$new_row .= '<ul class="thumbnails-default">';
	$new_row .= "\r\n\t\t\t\t\t\t\t";						

	// Loop through thumbs
	if ($dir = @opendir($pathSM)) {
		while (($file = readdir($dir)) !== false) {
			switch($file) {
				case ".DS_Store":
				break;
				case ".":
				break;
				case "..":
				break;
				default: 
				if ($cur == 7) {
					echo $new_row;
					$cur = 1;
				} 
				$count_fb_image ++;
				$cur++;
				print ' 
				<li class="columns two">
					<a href="'.$pathLG.'/'.$file.'" class="fancybox-thumbs" data-fancybox-group="thumb" >
						<img src="'.$pathSM.'/'.$file.'" alt="'.$file.'"  /> 
					</a>
				</li>';
				break;
			} // end switch
		}  // end while 
		closedir($dir);
	} // end if 
	
	return $fb_image;
}



function buildThumbnailsAnimateFoundPhotos() {

	// Variables
	$pathSM = "../_lib/images/animate-found-photos/_index";
	$fancybox_effect = 'fancybox-effects-c';
	$cur = 1;
	$count_fb_image = 1;
	$new_row = '</ul>';
	$new_row .= "\r\n\t\t\t\t\t";
	$new_row .= '</div>';
	$new_row .= "\r\n\t\t\t\t\t";
	$new_row .= '<div class="row">';
	$new_row .= "\r\n\t\t\t\t\t\t";
	$new_row .= '<ul class="thumbnails-default">';
	$new_row .= "\r\n\t\t\t\t\t\t\t";	

	$files = array();
	foreach (new DirectoryIterator($pathSM) as $fileInfo) {
	    if($fileInfo->isDot()) continue;
	    if ($fileInfo != '.' && $fileInfo != '..' && $fileInfo != '.DS_Store' && $fileInfo != 'Thumbs.db') {
		    $files[$fileInfo->getFilename()] = $fileInfo->getFilename();
		}
	}
	krsort($files);
	// echo '<pre>'; print_r($files); echo '</pre>';

	foreach ($files as $file) {
	    if ($cur == 7) {
			echo $new_row;
			$cur = 1;
		} 

		// remove file extension
		$pathInfo = pathinfo($file);
		$html_file_name = $pathInfo['filename'];

		$cur++;
	    print '

			<li class="columns two">
				<a href="#">
					'.$row_found_photos_animated['title'].'
				</a>
			</li>
		';
	} 
}

function buildThumbnailsAnimateFoundPhotosV1() {
	print "XXX";
	// Variables
	$pathSM = "../_lib/images/animate-found-photos/_index";
	$fancybox_effect = 'fancybox-effects-c';
	$cur = 1;
	$count_fb_image = 1;
	$new_row = '</ul>';
	$new_row .= "\r\n\t\t\t\t\t";
	$new_row .= '</div>';
	$new_row .= "\r\n\t\t\t\t\t";
	$new_row .= '<div class="row">';
	$new_row .= "\r\n\t\t\t\t\t\t";
	$new_row .= '<ul class="thumbnails-default">';
	$new_row .= "\r\n\t\t\t\t\t\t\t";	

	$files = array();
	foreach (new DirectoryIterator($pathSM) as $fileInfo) {
	    if($fileInfo->isDot()) continue;
	    if ($fileInfo != '.' && $fileInfo != '..' && $fileInfo != '.DS_Store' && $fileInfo != 'Thumbs.db') {
		    $files[$fileInfo->getFilename()] = $fileInfo->getFilename();
		}
	}
	krsort($files);
	// echo '<pre>'; print_r($files); echo '</pre>';

	foreach ($files as $file) {
	    if ($cur == 7) {
			echo $new_row;
			$cur = 1;
		} 

		// remove file extension
		$pathInfo = pathinfo($file);
		$html_file_name = $pathInfo['filename'];

		$cur++;
	    print ' 
		<li class="columns two thumb-wrapper">
			<a href="/animate-found-photos-'.$html_file_name.'/index.php" class="animate-fp-thumb cinemagraph-thumb-'.$html_file_name.' " data-fancybox-type="iframe">
				
			</a>
		</li>';
	} 
}
?>