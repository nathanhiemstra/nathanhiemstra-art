<?
function getImagesInDirectory($project_name) {
	// Variables
	$pathSM = "../_lib/images/".$project_name."";
	$fancybox_effect = 'fancybox-effects-c';
	if ($project_name == 'circus-wagon-wheels')  {
		$fancybox_effect = 'fancybox-effects-c2';
	}
	$cur = 1;
	$count_fb_image = 1;
		

	$files = array();
	foreach (new DirectoryIterator($pathSM) as $fileInfo) {
	    if($fileInfo->isDot()) continue;
	    if ($fileInfo != '.' && $fileInfo != '..' && $fileInfo != '.DS_Store' && $fileInfo != 'Thumbs.db') {
		    $files[$fileInfo->getFilename()] = $fileInfo->getFilename();
		}
	}
	krsort($files);
	// echo '<pre>'; print_r($files); echo '</pre>';

	foreach ($files as $file) {

		$cur++;
	    print ' 
		<img src="'.$pathSM.'/'.$file.'" alt="'.$file.'"  /> ';
	} 
}
?>