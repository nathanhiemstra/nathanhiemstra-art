/* ==========================================================================
   Table of Contents
     - Fancybox Defaults
   ========================================================================== */


/* ==========================================================================
 Animate Found Photos
 ========================================================================== */
(function(animateFP, $, undefined) {

  var containerClassName = '.animate-found-photos',
    container = $(containerClassName),
    containerContainer,
    allButFirstImage,
    allButFirstAndLastImage,
    controlsSpeedFader,
    speedFade = 500,
    speedFadeSlowest = 1000,
    speedFadeFastest = 5,
    speedInterval = 50,
    speedIntervalSlowest = 1000,
    speedIntervalFastest = 0,
    sliderValue,
    containerWidth,
    containerHeight,
    speedFadeRange;

  animateFP.util = {
    init: function(customValues) {



      // animateFP.util.initializeCustomValues(customValues);
      //       animateFP.util.initializeControls();
      // animateFP.util.getMousePosition();
      // animateFP.util.resetImages();
      // animateFP.util.nextImage(); 


      // $('.photos',container).mouseenter(function() {
      //   // Note that the photo has been moused over 
      //     $(container).attr('data-controls-speed-fade-last-updated','hover-photo');
      // });

      $(controlsSpeedFader, container).click(function() {
        $(container).attr('data-controls-speed-fade-last-updated', 'drag-slider');
      });

      $(controlsSpeedFader).change(function() {

        console.log('change');

        if ($(container).data('controls-speed-fade-last-updated', 'drag-slider')) {

          // Find out the new position/value of the slider
          sliderValue = $(controlsSpeedFader).val();
        }

        // Update the fade speed by using the new slider position/value
        speedFade = parseInt(speedFadeRange) - parseInt(sliderValue);

      });
    },
    initializeCustomValues: function(customValues) {

      // See if any custom variables have been passed in

      // Elements
      if (customValues.containerContainer) {
        container = $(containerClassName, containerContainer);
      }
      allButFirstImage = $('img:not(:first-child)', container);
      allButFirstAndLastImage = $('img:not(:first-child):not(:last-child)', container);
      controlsSpeedFader = $('.controls .speed-fade', container);

      // Values based on elements defined above
      sliderValue = $(controlsSpeedFader).val();
      containerWidth = $(container).width();
      containerHeight = $(container).height();

      // Custom values
      if (customValues.speedFade) {
        speedFade = customValues.speedFade;
      }
      if (customValues.speedFadeSlowest) {
        speedFadeSlowest = customValues.speedFadeSlowest;
      }
      if (customValues.speedFadeFastest) {
        speedFadeFastest = customValues.speedFadeFastest;
      }
      if (customValues.speedInterval) {
        speedInterval = customValues.speedInterval;
      }

      // Values based on custom values defined above
      speedFadeRange = speedFadeSlowest + speedFadeFastest;
    },
    initializeControls: function() {
      $(controlsSpeedFader).attr('min', speedFadeFastest).attr('max', speedFadeSlowest);
    },
    updateControlFadeSlider: function(newSliderValue) {
      $(controlsSpeedFader).val(newSliderValue).trigger('change');
    },
    getMousePosition: function() {
      $('.photos', this.container).bind('mousemove', function(ev) {
        var newSliderValue;
        var $this = $(this),
          tPos = $this.offset(),
          mousePos = {
            x: ev.pageX,
            y: ev.pageY
          };

        mousePos.x -= tPos.left + parseInt($this.css('paddingLeft')) + parseInt($this.css('borderLeftWidth'));
        mousePos.y -= tPos.top + parseInt($this.css('paddingTop')) + parseInt($this.css('borderTopWidth'));
        // console.log('x = ' + mousePos.x + ', y = ' + mousePos.y);
        newSliderValue = (mousePos.x / containerWidth) * speedFadeSlowest;
        animateFP.util.updateControlFadeSlider(newSliderValue);
      });
    },
    resetImages: function() {
      allButFirstImage.hide();
    },
    nextImage: function() {
      console.log('nextImage: ');
      if ($('img:last-child', container).is(':hidden')) {
        animateFP.util.fadeInNextImage();
      } else {
        allButFirstAndLastImage.hide();
        animateFP.util.fadeOutLastImage();
      }
      $("img", container).promise().done(function() {
        setTimeout(animateFP.util.nextImage, speedInterval);
      });
    },
    fadeInNextImage: function() {
      $("img:hidden:first", container).fadeIn(speedFade);
    },
    fadeOutLastImage: function() {
      $("img:last", container).fadeOut(speedFade);
    },
    changeSpeedFade: function() {
      sliderValue = $(controlsSpeedFader).val();
      console.log('sliderValue: ', sliderValue);
      return speedFade = speedFadeRange - parseInt(sliderValue);
    },
    zoomPositionNext: function() {
      var $target = $('.animate-found-photos #photo');
      var positionCurrent = 0;
      var positionsTotal = $target.attr('zoom-positions');

    }
  }

}(window.animateFP = window.animateFP || {}, jQuery));



$(document).ready(function() {


  /* ==========================================================================
  Addio Soundboard - Long
  ========================================================================== */
  audioSoundboardLong = function() {
    var parentSelector = $('.audio-soundboard-long');

    var pauseOtherAudioItem = function(clickedItem) {
      $(clickedItem).addClass('clicked');
      $('audio').each(function() {
        // If this is playing...
        if (!this.paused) {
          // Pause the one that's playing that is not the one we just clicked
          if (!$(this).hasClass("clicked")) {
            this.pause();
          }
        }
      });
      $(clickedItem).removeClass('clicked');
    }

    // Prevent default on "hide" link
    $(".transcript-trigger a", parentSelector).click(function(event) {
      event.preventDefault();
    });
    // Open/Close lyrics container
    $(".transcript-trigger", parentSelector).click(function() {
      $(this).parent(".transcript").slideToggle("fast");
      event.stopPropagation();
    });
    // Prevent default on "hide" link
    $(".item", parentSelector).click(function(event) {
      if ($(this).hasClass('played')) {
        $(this).find(".transcript").slideToggle("fast");
      }
    });

    // Set up each audio file
    $("audio", parentSelector).each(function(i, audioElement) {
      var audio = $(this),
        that = this //closure to keep reference to current audio tag
        ,
        src = $("source", this).attr("src"),
        parent = $(this).parent(".item"),
        transcript = $(parent).find(".transcript");

      // Show all transcripts
      $(".transcripts-all-show", parentSelector).click(function() {
        $(".transcript", parentSelector).slideDown("fast");
      });

      // Hide all transcripts
      $(".transcripts-all-hide").click(function() {
        $(".transcript").slideUp("fast");
      });

      // When starts playing, make the transcript change visually
      $(this).on('play', function() {
        $(parent).addClass("played").addClass("playing");
        if ($(transcript).is(':hidden')) {
          $(transcript).slideDown("fast");
        }
        pauseOtherAudioItem(this);
      });

      // When paused, make the transcript change visually
      $(this).on('pause', function() {
        $(parent).removeClass("playing");
      });

      // When done playing, make transcript change visually
      $(this).on('ended', function() {
        $(transcript).slideUp("fast");
        $(parent).removeClass("playing");
      });

      // Set up buttons and add pause/play logic
      $(that).click(function() {


        // Check all the audio files to see if they are currently playing
        $('audio', parentSelector).each(function() {

          // If this is playing...
          if (!this.paused) {
            // Pause the one that's playing that is not the one we just clicked
            if (!$(this).hasClass("clicked")) {
              this.pause();
            }
          }
        });
        $(audio).removeClass('clicked');
      });
    });
  } // end audioSoundboardLong()




  /* ==========================================================================
   Navigation - Next / Prev
   ========================================================================== */

  nextPrevViaArrowKeys = function() {
    $(document).keydown(function(e) {
      // Left arrow
      if (e.keyCode == 37) {
        $('#nav-page-prev')[0].click();
        return false;
      } else if (e.keyCode == 39) {
        $('#nav-page-next')[0].click();
        return false;
      }
    });
  }

  /* ==========================================================================
   Navigation - Mobile
   ========================================================================== */

  // Hide on page load
  // $('.nav-main').hide();
  // $('.nav-main .sub').hide();

  // Open/close navigation by clicking menu icon
  $(".nav-main-trigger").on("click", function() {
    if ($(this).hasClass('selected')) {
      $(this).removeClass('selected');
      $('.nav-main').slideUp('fast');
    } else {
      $(this).addClass('selected');
      $('.nav-main').slideDown('fast');
    }
  });


  // Open/close sub navigation
  $(".nav-main > li > a").click(function(event) {
    var parent = $(this).parents('li');
    event.preventDefault();
    $('.sub', parent).addClass('clicked');
    $('.sub:not(.clicked)').slideUp('fast');
    $('.sub', parent).slideToggle('fast');
    $('.sub', parent).removeClass('clicked');
  });

  $(document).keydown(function(e) {
    // Left arrow
    if (e.keyCode == 27) {
      $('.nav-main .sub').hide();
      return false;
    }
  });


  /* ==========================================================================
  Navigation - Desktop
  ========================================================================== */

  // $('.nav-main > li').mouseenter(function() {
  //   $('.nav-main li').removeClass('show');
  //   $(this).addClass('show');
  // });

  // $('.nav-main > li').mouseleave(function() {
  //   $('.nav-main li').removeClass('show');
  // });


  /* ==========================================================================
   Fancybox Defaults
   ========================================================================== */


  $('.fancybox').fancybox();

  /*
   *  Different effects
   */

  // Change title type, overlay closing speed
  $(".fancybox-effects-a").fancybox({
    helpers: {
      title: {
        type: 'outside'
      },
      overlay: {
        speedOut: 0
      }
    }
  });

  // Disable opening and closing animations, change title type
  $(".fancybox-effects-b").fancybox({
    openEffect: 'none',
    closeEffect: 'none',

    helpers: {
      title: {
        type: 'over'
      }
    }
  });

  // Set custom style, close if clicked, change title type and overlay color
  $(".fancybox-effects-c").fancybox({
    wrapCSS: 'fancybox-custom',
    closeClick: true,
    autoScale: true,
    closeBtn: false,
    padding: 0,
    margin: 5,

    openEffect: 'none',
    prevEffect: 'none',
    nextEffect: 'none',


    helpers: {
      title: {
        type: 'inside'
      },
      overlay: {
        css: {
          'background': 'rgba(0,0,0,0.90)'
        }
      }
    }
  });

  // Set custom style, close if clicked, change title type and overlay color
  $(".fancybox-effects-c2").fancybox({
    prevEffect: 'none',
    nextEffect: 'none',

    openEffect: 'none',

    helpers: {
      title: {
        type: 'outside'
      },
      overlay: {
        speedOut: 0
      }
    }
  });

  // Remove padding, set opening and closing animations, close if clicked and disable overlay
  $(".fancybox-effects-d").fancybox({
    padding: 0,

    openEffect: 'elastic',
    openSpeed: 150,

    closeEffect: 'elastic',
    closeSpeed: 150,

    closeClick: true,

    helpers: {
      overlay: null
    }
  });

  /*
   *  Button helper. Disable animations, hide close button, change title type and content
   */

  $('.fancybox-buttons').fancybox({
    openEffect: 'none',
    closeEffect: 'none',

    prevEffect: 'none',
    nextEffect: 'none',

    closeBtn: false,

    helpers: {
      title: {
        type: 'inside'
      },
      buttons: {}
    },

    afterLoad: function() {
      this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
    }
  });


  /*
   *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
   */

  $('.fancybox-thumbs').fancybox({
    prevEffect: 'none',
    nextEffect: 'none',

    closeBtn: false,
    arrows: true,
    nextClick: true,

    helpers: {
      thumbs: {
        width: 40,
        height: 40
      }
    }
  });

  $(".various").fancybox({
    maxWidth: 800,
    maxHeight: 600,
    fitToView: false,
    width: '70%',
    height: '70%',
    autoSize: false,
    closeClick: false,
    openEffect: 'none',
    closeEffect: 'none'
  });

  /*
   *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
   */
  $('.fancybox-media')
    .attr('rel', 'media-gallery')
    .fancybox({
      openEffect: 'none',
      closeEffect: 'none',
      prevEffect: 'none',
      nextEffect: 'none',

      arrows: false,
      helpers: {
        media: {},
        buttons: {}
      }
    });

});