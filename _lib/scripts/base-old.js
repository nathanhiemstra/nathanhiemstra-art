/* ==========================================================================
	 Table of Contents
	 	- Fancybox Defaults
	 ========================================================================== */


$(document).ready(function() {




	/* ==========================================================================
	 Animate Found Photos
	 ========================================================================== */
	
	animateFoundPhotos.init();

	animateFoundPhotos = {

		container 				: $('.animate-found-photos'),
		allButFirstImage 		: $('img:not(:first-child)',this.container),
		allButFirstAndLastImage : $('img:not(:first-child):not(:last-child)',this.container),
		controlsSpeedFader 		: $('.controls .speed-fade',this.container),
		speedFade 				: 500,
		speedInterval 			: 50,
		speedFadeSlowest 		: 1000,
		speedFadeFastest 		: 5,
		speedIntervalSlowest 	: 1000,
		speedIntervalFastest 	: 0,
		speedFadeRange : function(){
	        return speedFadeRange = this.speedFadeSlowest + this.speedFadeFastest;   
	    },
		sliderValue : function(){
	        return $(this.controlsSpeedFader).val();   
	    },
	    containerWidth : function(){
	        return $(this.container).width() 
	    },
	    containerHeight : function(){
	        return $(this.container).height() 
	    },


		init: function(){

			// Initiate
	        this.resetImages();
	        this.nextImage(); 
	        this.initializeControls();
	        this.getMousePosition();

	        $('.photos',this.container).mouseenter(function() {
	        	// Note that the photo has been moused over 
	            $(this.container).attr('data-controls-speed-fade-last-updated','hover-photo');
	        });

	        $(this.controlsSpeedFader,this.container).click(function() {
	        	$(animateFoundPhotos.container).attr('data-controls-speed-fade-last-updated','drag-slider');
	        });

	        $(this.controlsSpeedFader).change(function() {
	            if ($(animateFoundPhotos.container).data('controls-speed-fade-last-updated','drag-slider')) {
	                
	                // Find out the new position/value of the slider
	                animateFoundPhotos.sliderValue();
	            } 
	            
	            // Update the fade speed by using the new slider position/value
	            animateFoundPhotos.speedFade = parseInt(animateFoundPhotos.speedFadeRange()) - parseInt(animateFoundPhotos.sliderValue());
	        });

	        
	    },
	    changeSpeedFade: function() {
	        this.sliderValue = $(this.controlsSpeedFader).val();
	        return speedFade = this.speedFadeRange() - parseInt(this.sliderValue);
	    },
	    initializeControls: function() {
	        $(this.controlsSpeedFader).attr('min',this.speedFadeFastest).attr('max',this.speedFadeSlowest);
	    },
	    resetImages: function() {
	        this.allButFirstImage.hide();
	    },
	    fadeInNextImage: function() {
	        $( "img:hidden:first",this.container).fadeIn(this.speedFade);
	    },
	    fadeOutLastImage: function() {
	        $( "img:last",this.container).fadeOut(this.speedFade);
	    },
	    nextImage: function() {
	        if ($('img:last-child',this.container).is(':hidden')) {
	          	animateFoundPhotos.fadeInNextImage();
	        } else {
	          	animateFoundPhotos.allButFirstAndLastImage.hide();
	          	animateFoundPhotos.fadeOutLastImage();
	        }
	        $( "img",this.container).promise().done(function() {
	        	var thisLocal = animateFoundPhotos;
	        	setTimeout(thisLocal.nextImage, thisLocal.speedInterval);
	        });
	    },
	    updateControlFadeSlider: function(newSliderValue) {
	        $(this.controlsSpeedFader).val(newSliderValue).trigger('change');

	    },
	    getMousePosition: function() {
	        $('.photos',this.container).bind('mousemove', function(ev){
	            var newSliderValue;
	            var $this = $(this),
	                 tPos = $this.offset(),
	             mousePos = {x: ev.pageX, y: ev.pageY};
	            
	            mousePos.x -= tPos.left + parseInt($this.css('paddingLeft')) + parseInt($this.css('borderLeftWidth'));
	            mousePos.y -= tPos.top + parseInt($this.css('paddingTop')) + parseInt($this.css('borderTopWidth'));
	            // console.log('x = ' + mousePos.x + ', y = ' + mousePos.y);
	            newSliderValue = (mousePos.x / animateFoundPhotos.containerWidth()) * animateFoundPhotos.speedFadeSlowest;
	            animateFoundPhotos.updateControlFadeSlider(newSliderValue);
	        });
	    }
	}

    // animateFoundPhotos.init();



	
	


	// function animateFoundPhotos() {
	// 	// Initiate
	// 	resetImages();
	// 	nextImage(); 
	// 	initializeControls();
	// 	getMousePosition();

	// 	$('.photos',container).mouseenter(function() {
	// 		$(container).attr('data-controls-speed-fade-last-updated','hover-photo');
	// 	});

	// 	(controlsSpeedFader,container).click(function() {
	// 		$(container).attr('data-controls-speed-fade-last-updated','drag-slider');
	// 	});
		


	// 	$(controlsSpeedFader).change(function() {
	// 		if ($(container).data('controls-speed-fade-last-updated','drag-slider')) {
	// 			// console.log('drag');
	// 			sliderValue = $(controlsSpeedFader).val();
	// 		} 
			
	// 		speedFade = speedFadeRange - parseInt(sliderValue);
	// 		// console.log('controlsSpeedFader');
	// 	});
	// }
	// function changeSpeedFade() {
	// 	sliderValue = $(controlsSpeedFader).val();
	// 	speedFade = speedFadeRange - parseInt(sliderValue);
	// }

	// function initializeControls() {
	// 	$(controlsSpeedFader).attr('min',speedFadeFastest).attr('max',speedFadeSlowest);
	// }
	
	// function resetImages() {
	// 	allButFirstImage.hide();
	// }
	// function fadeInNextImage() {
	// 	$( "img:hidden:first",container).fadeIn(speedFade);
	// }

	// function fadeOutLastImage() {
	// 	$( "img:last",container).fadeOut(speedFade);
	// }

	// function nextImage() {
	// 	if ($('img:last-child',container).is(':hidden')) {
	// 	  fadeInNextImage();
	// 	} else {
	// 	  allButFirstAndLastImage.hide();
	// 	  fadeOutLastImage();
	// 	}
	// 	// Once finished, call this function again
	// 	$( "img",container).promise().done(function() {
	// 	  setTimeout(nextImage, speedInterval);
	// 	});
	// }

	// function updateControlFadeSlider(newSliderValue) {
	// 	$(controlsSpeedFader).val(newSliderValue).trigger('change');
	// }

	// function getMousePosition() {
	// 	$('.photos',container).bind('mousemove', function(ev){
	// 	    var $this = $(this),
	// 	         tPos = $this.offset(),
	// 	     mousePos = {x: ev.pageX, y: ev.pageY};
		    
	// 	    mousePos.x -= tPos.left + parseInt($this.css('paddingLeft')) + parseInt($this.css('borderLeftWidth'));
	// 	    mousePos.y -= tPos.top + parseInt($this.css('paddingTop')) + parseInt($this.css('borderTopWidth'));

	// 	    // console.log('x = ' + mousePos.x + ', y = ' + mousePos.y);

	// 	    newSliderValue = (mousePos.x / containerWidth) * speedFadeSlowest;
	// 	    updateControlFadeSlider(newSliderValue);
	// 	});
	// }

	// Initiate Animation of Found Photos
	// animateFoundPhotos.init();







	 /* ==========================================================================
	 Addio Soundboard - Long
	 ========================================================================== */
	audioSoundboardLong = function () {
		var parentSelector = $('.audio-soundboard-long');

		var pauseOtherAudioItem = function(clickedItem) {
			$(clickedItem).addClass('clicked');
			$('audio').each(function() {
				// If this is playing...
				if(!this.paused){
					// Pause the one that's playing that is not the one we just clicked
					if (!$(this).hasClass("clicked")) {
						this.pause();
					}
				}
			});
			$(clickedItem).removeClass('clicked');
		}

		// Prevent default on "hide" link
		$(".transcript-trigger a",parentSelector).click(function(event) {
			event.preventDefault();
		});
		// Open/Close lyrics container
		$(".transcript-trigger",parentSelector).click(function() {
			$(this).parent(".transcript").slideToggle("fast");
			event.stopPropagation();
		});
		// Prevent default on "hide" link
		$(".item",parentSelector).click(function(event) {
			if ($(this).hasClass('played')) {
				$(this).find(".transcript").slideToggle("fast");
			}
		});

		// Set up each audio file
		$("audio",parentSelector).each(function(i, audioElement) {
			var audio = $(this)
				, that = this //closure to keep reference to current audio tag
				, src = $("source",this).attr("src")
				, parent = $(this).parent(".item")
				, transcript = $(parent).find(".transcript");

			// Show all transcripts
			$(".transcripts-all-show",parentSelector).click(function() {
				$(".transcript",parentSelector).slideDown("fast");
			});

			// Hide all transcripts
			$(".transcripts-all-hide").click(function() {
				$(".transcript").slideUp("fast");
			});

			// When starts playing, make the transcript change visually
			$(this).on('play', function(){
				$(parent).addClass("played").addClass("playing");
				if ($(transcript).is(':hidden')) {
					$(transcript).slideDown("fast");
				}
				pauseOtherAudioItem(this);
			});

			// When paused, make the transcript change visually
			$(this).on('pause', function(){
				$(parent).removeClass("playing");
			});

			// When done playing, make transcript change visually
			$(this).on('ended', function(){
				$(transcript).slideUp("fast");
				$(parent).removeClass("playing");
			});

			// Set up buttons and add pause/play logic
			$(that).click(function() {
				

				// Check all the audio files to see if they are currently playing
				$('audio',parentSelector).each(function() {

					// If this is playing...
					if(!this.paused){
						// Pause the one that's playing that is not the one we just clicked
						if (!$(this).hasClass("clicked")) {
							this.pause();
						}
					}
				});
				$(audio).removeClass('clicked');
			});
		});
	} // end audioSoundboardLong()


	/* ==========================================================================
	 Navigation - Mobile
	 ========================================================================== */

	 // Hide on page load
	 // $('.nav-main').hide();
	 // $('.nav-main .sub').hide();
	 
	 // Open/close navigation by clicking menu icon
	 $('.nav-main-trigger').click(function(event) {
		event.preventDefault();
		if ($(this).hasClass('selected')) {
			$(this).removeClass('selected');
			$('.nav-main').slideUp('fast');
		} else {
			$(this).addClass('selected');
			$('.nav-main').slideDown('fast');
		}
	});


	 // Open/close sub navigation
	 $(".nav-main > li > a").click(function(event) {
		var parent = $(this).parents('li');
		event.preventDefault();
		$('.sub',parent).addClass('clicked');
		$('.sub:not(.clicked)').slideUp('fast');
		$('.sub',parent).slideToggle('fast');
		$('.sub',parent).removeClass('clicked');
	});

	 
	 /* ==========================================================================
	 Navigation - Desktop
	 ========================================================================== */

	 // $('.nav-main > li').mouseenter(function() {
	 // 	$('.nav-main li').removeClass('show');
	 // 	$(this).addClass('show');
	 // });

	 // $('.nav-main > li').mouseleave(function() {
	 // 	$('.nav-main li').removeClass('show');
	 // });


	/* ==========================================================================
	 Fancybox Defaults
	 ========================================================================== */


	$('.fancybox').fancybox();

	/*
	 *  Different effects
	 */

	// Change title type, overlay closing speed
	$(".fancybox-effects-a").fancybox({
		helpers: {
			title : {
				type : 'outside'
			},
			overlay : {
				speedOut : 0
			}
		}
	});

	// Disable opening and closing animations, change title type
	$(".fancybox-effects-b").fancybox({
		openEffect  : 'none',
		closeEffect	: 'none',

		helpers : {
			title : {
				type : 'over'
			}
		}
	});

	// Set custom style, close if clicked, change title type and overlay color
	$(".fancybox-effects-c").fancybox({
		wrapCSS    : 'fancybox-custom',
		closeClick : true,

		openEffect : 'none',

		helpers : {
			title : {
				type : 'inside'
			},
			overlay : {
				css : {
					'background' : 'rgba(238,238,238,0.85)'
				}
			}
		}
	});

	// Set custom style, close if clicked, change title type and overlay color
	$(".fancybox-effects-c2").fancybox({
		prevEffect : 'none',
		nextEffect : 'none',

		openEffect : 'none',

		helpers: {
			title : {
				type : 'outside'
			},
			overlay : {
				speedOut : 0
			}
		}
	});

	// Remove padding, set opening and closing animations, close if clicked and disable overlay
	$(".fancybox-effects-d").fancybox({
		padding: 0,

		openEffect : 'elastic',
		openSpeed  : 150,

		closeEffect : 'elastic',
		closeSpeed  : 150,

		closeClick : true,

		helpers : {
			overlay : null
		}
	});

	/*
	 *  Button helper. Disable animations, hide close button, change title type and content
	 */

	$('.fancybox-buttons').fancybox({
		openEffect  : 'none',
		closeEffect : 'none',

		prevEffect : 'none',
		nextEffect : 'none',

		closeBtn  : false,

		helpers : {
			title : {
				type : 'inside'
			},
			buttons	: {}
		},

		afterLoad : function() {
			this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
		}
	});


	/*
	 *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
	 */

	$('.fancybox-thumbs').fancybox({
		prevEffect : 'none',
		nextEffect : 'none',

		closeBtn  : false,
		arrows    : true,
		nextClick : true,

		helpers : {
			thumbs : {
				width  : 40,
				height : 40
			}
		}
	});

	/*
	 *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
	*/
	$('.fancybox-media')
		.attr('rel', 'media-gallery')
		.fancybox({
			openEffect : 'none',
			closeEffect : 'none',
			prevEffect : 'none',
			nextEffect : 'none',

			arrows : false,
			helpers : {
				media : {},
				buttons : {}
			}
		});

});
