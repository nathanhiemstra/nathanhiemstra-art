#!/usr/bin/env bash
echo "--------------- ".$BASH_VERSION


cd /Users/nathanhiemstra/Sites/nathanhiemstra.com/nathanhiemstra-art/
# ./_lib/scripts/bash/new-cinemagraph.sh fireworks-from-boat "Fireworks From Boat"


function prompt() {
  osascript <<EOT
    tell app "System Events"
      text returned of (display dialog "$1" default answer "$2" buttons {"OK"} default button 1 with title "$(basename $0)")
    end tell
EOT
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ARGUMENTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



# Get from terminal
# new_photo_filename=$1
# new_photo_title=$2
add_to_database=$3
# Change _ to spaces
new_photo_title=$new_photo_title | sed -e 's/_/ /g'


# Get from dialog window
new_photo_title="$(prompt 'Title:' '')"
new_photo_filename="$(prompt 'File name:' '')"



# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# INPUT IMAGE 
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# - PARENT

input_parent_folder="../nathanhiemstra-art-source-files/_lib/images/cinemagraphs/_originals/"$new_photo_filename
echo "input_parent_folder: ".$input_parent_folder;

if [ -d $input_parent_folder ]; then
    echo ""
else
    echo ""
    echo "Uhh, can't find: "$input_parent_folder 
    echo ""
    exit
fi

# - MAIN
input_main_folder=$input_parent_folder"/main/"

if [ -d $input_main_folder ]; then
    echo ""
else
    echo ""
    echo "Uhh, can't find: "$input_main_folder 
    echo ""
    exit
fi

# - SQUARE
input_square_folder=$input_parent_folder"/square/"

if [ -d $input_square_folder ]; then
    echo ""
else
    echo ""
    echo "Uhh, can't find: "$input_square_folder 
    echo ""
    exit
fi

sample_image=$input_main_folder"1.jpg"

if [ -f $sample_image ]; then
    echo ""
else
    echo ""
    echo "Uhh, can't find: "$sample_image 
    echo ""
    exit
fi

echo " "
echo "sample_image: ".$sample_image

# Dimensions - Input
input_width=`identify -ping -format "%[fx:w]" $sample_image`
input_height=`identify -ping -format "%[fx:h]" $sample_image`

# Dimensions - Output
output_main_settings="-resize 1366 -quality 70"
is_vertical="0"
if [ $input_width -le $input_height ]; then
    output_main_settings="-resize x1080 -quality 70"
    is_vertical="1"
fi
output_video_frame_main_settings="-resize x1080"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# OUTPUT IMAGE 
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# - MAIN
output_parent_folder="_lib/images/cinemagraphs/"$new_photo_filename
output_main_folder=$output_parent_folder"/"

# Number of frames (count files)            
number_of_frames=$( find ${input_main_folder}*.jpg -type f | wc -l | sed 's/^[[:space:]]*//');

# - SPRITE
output_sprite_folder="_lib/images/cinemagraphs/"$number_of_frames"-frames/"
output_sprite_folder_temp=$output_parent_folder"/icons_temp/"
output_sprite=$output_sprite_folder$new_photo_filename".jpg"
output_sprite_settings="-resize 280x280^ \
			  		  -gravity Center \
			  		  -crop 280x280+0+0 +repage"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# DATABASE
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
local_database_host="localhost";
local_database_credentials="-uroot -proot";
local_database_name="nathanhiemstra_art_portfolio";
local_mysql_path="/Applications/MAMP/Library/bin/mysql";    
database_table_name="found_photos_animated"
                  
  
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# VALIDATE
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Make sure input folder exists
if [ $new_photo_filename == "" ]; then
    echo ""
    echo "Uhh, you forgot to give the photo an id." 
    echo ""
    exit
fi

if [ $new_photo_title == "" ]; then
    echo ""
    echo "Uhh, you forgot to give the photo name." 
    echo ""
    exit
fi

if [ $number_of_frames == "" ]; then
    echo ""
    echo "Uhh, how many frames?" 
    echo ""
    exit
fi


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# BUILD IT!
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
echo ""
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" 
echo "Making the new photo: '$new_photo_filename' -  $number_of_frames frames -  w:"$input_width"px h:"$input_height"px"


# Delete previous version of image folder
rm -rf $output_parent_folder

# Create new folders we'll need
mkdir $output_parent_folder


# MAIN
echo "MAIN:"
echo "    $output_main_folder"
cp -r $input_main_folder  $output_main_folder

# - Resize  
echo "    "$output_main_settings
for (( i=1; i<=$number_of_frames; i++ ))
do  
   convert $input_main_folder$i".jpg" $output_main_settings  $output_main_folder$i".jpg"
done


# SPRITE
echo "SPRITE:"
echo "    $output_sprite_folder_temp"
cp -r $input_square_folder  $output_sprite_folder_temp

# Resize  
echo "    "$output_sprite_settings
for (( i=1; i<=$number_of_frames; i++ ))
do  
   convert $output_sprite_folder_temp$i".jpg" $output_sprite_settings  $output_sprite_folder_temp$i".jpg"
done

# Merge into one image 
echo "     Merging icon images into one image:"
echo "     $output_sprite"
convert +append $output_sprite_folder_temp"/"*.jpg $output_sprite 

# Delete temp folder
echo "     Delete temp folder: $output_sprite_folder_temp"
rm -rf $output_sprite_folder_temp 

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# SQL INSERT STATEMENT:
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

echo "DATABASE INSERT"
echo "     Getting order number..."
highest_order_number=$($local_mysql_path --host=$local_database_host $local_database_credentials $local_database_name -s -N -e "SELECT \`order\` FROM  $database_table_name ORDER BY \`order\` DESC LIMIT 1")
order_number=$(($highest_order_number + 1))

echo "     Building SQL staement..."

sql_insert_header="INSERT INTO \`$database_table_name\` (\`title\`, \`filename\`, \`frames\`, \`vertical\`, \`order\`, \`sql_insert\`) VALUES ('${new_photo_title}','$new_photo_filename','$number_of_frames','$is_vertical','$order_number','"
sql_insert_footer="');"
# Make a second insert to store in the db table. Backslash the singlw quotes
sql_insert_entry_header="INSERT INTO \`$database_table_name\` (\`title\`, \`filename\`, \`frames\`, \`vertical\`, \`order\`, \`sql_insert\`) VALUES (\'${new_photo_title}\',\'$new_photo_filename\',\'$number_of_frames\',\'$is_vertical\',\'$order_number\',\'"
sql_insert_entry_footer="\');"
sql_insert=$sql_insert_header$sql_insert_entry_header$sql_insert_entry_footer$sql_insert_footer

if [ "$add_to_database" != "no" ]; then  
    echo "     Inserting:"
    echo "                 "$sql_insert
    echo ""
    echo "     Into: '"$local_database_host" > "$local_database_name"'"
    # Insert into local database
    $local_mysql_path --host=$local_database_host $local_database_credentials $local_database_name << EOF
    $sql_insert
EOF
fi
echo ""


echo "Done. NOW PAY ME."
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo ""