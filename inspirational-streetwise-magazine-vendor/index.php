<?
	// variables
	$title = "The Inspirational StreetWise Magazine Vendor";
	$section_name = "audio";
	$project_name = "inspirational-streetwise-magazine-vendor";
	$page_name = "inspirational-streetwise-magazine-vendor";
	$description = "I'm moved my the happiness and optimism of this man standing out in the cold selling magazines.";
	$initialize_js_funtion = "audioSoundboardLong()";
	$i = NULL;

	include "../inc/header-html.php";
?>
</head>
<body class="p-<?=$project_name?> audio-soundboard-long ">
	<? include "../inc/header.php"; ?>
	<div class="row p-<?=$project_name?>">
		<header role="banner">
			<div class="container hd clearfix">
				<div class="columns nine">
					<h1>The Inspirational <span>StreetWise Magazine Vendor</span> </h1>
					<h2>“Hot off the press, warm to the touch”</h2>
				</div>
				<div class="columns three">
					<a href="/_lib/images/<?=$project_name?>/portrait-lg.png" class="fancybox-effects-c" title="On the way to work I'd stand by Keith while waiting for the light to change. After a few months, his catchy lines and positive disposition became endearing and I looked forward to hearing what he had to say.">
						<img src="/_lib/images/<?=$project_name?>/portrait.png" class="portrait">
					</a>
				</div>
			</div><!-- end .container -->

		</header><!-- end #hd -->
	</div>

	<div class="container hd" id="main">
		<div class="row">
			<div class="columns twelve audio-container">
				<?
					$json_file = file_Get_contents("../_lib/json/".$project_name."/audio-files.json");
					$json = json_decode($json_file);
					$total_entries = 0;
					foreach($json->entries as $record){
						$total_entries++;
					}
					$entries_per_column = round($total_entries / 4);
				?>
				<div class="columns three">
					<ul>
						<?
							// Make columns

							foreach($json->entries as $record){
								if ($i == $entries_per_column) {
									print '</ul></div><div class="columns three"><ul >';
									$i = 0;
								}
								$i++;
						?>
						<li class="item">
							<div class="clearfix">
								<span class="date">
									<span><?=date("l, ",strtotime($record->date))?></span>
									<?=date("F j, Y",strtotime($record->date))?>
								</span>
								<h3 class="title"><?=$record->title?></h3>
								<? /*<span class="duration"><?=$record->duration;?></span> */ ?>
							</div>

							<audio controls preload="metadata" title="<?=$record->title?> <span><?=$record->duration;?></span>">
								<source src="/_lib/audio/<?=$project_name?>/mp3/<?=$record->filename?>.mp3" type="audio/mpeg">
								<source src="/_lib/audio/<?=$project_name?>/ogg/<?=$record->filename?>.ogg" type="audio/ogg">
							</audio>
							<? if ($record->lyric) { ?>
								<div class="transcript" >
									<p class="transcript-content" id="transcript-<?=$i?>"><?=$record->lyric?></p>
									<p class="transcript-trigger"><a href="#transcript-<?=$i?>">Hide</a></p>
								</div>
							<? } ?>
						</li>
						<? } ?>
					</ul>
				</div>
			</div>
		</div>

		<div class="row ">
			<p>
				<a href="#" class="transcripts-all-show">Show  transcipts</a>
				<a href="#" class="transcripts-all-hide">Hide  transcipts</a>
			</p>
		</div>

		<div class="row ">
			<p>
				On my way to work every morning, while waiting for the crosswalk light, I stood next to this man, selling magazines. After months of him just being part of the morning city sounds, one morning his catchphrases “hot off the press, warm to the touch” broke into my focus and it delighted me. Then I became fascinated with the rest of his catchphrases. No matter how cold, dreary, or tired his feet may have been, his words and disposition were warm and positive. 
			</p>
			<p>
				I have an office job and have to find time to make art. This is one in a series of pieces I do while on my way to work, documenting recurring people and objects.
			</p>
		</div>



		

<? /*
		<div class="columns twelve">

			<div id="disqus_thread"></div>
		    <script type="text/javascript">
		        // CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE
		        var disqus_shortname = 'mikebike'; // required: replace example with your forum shortname

		        // DON'T EDIT BELOW THIS LINE
		        // (function() {
		        //     var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
		        //     dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
		        //     (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
		        // })();
		    </script>
		    <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>

		</div>
*/?>
	</div><!-- end #main -->


<?php include "../inc/footer.php"; ?>
