<?
	// variables
	$title = "Nathan Hiemstra - Art Portfolio";
	$project_name = "home";
	$page_name = "home";
	$section_name = "home";
	$description = "Artist portfolio : Drawings, Photographs, Social Practice, Audio, Video, Sculpture - Chicago, IL";
	$hide_active = "AND project.active = 'true' AND category.active = 'true'";
	// Had to comment out for PHP 7. Come back to.
	// if ($_GET['show'] != 'all') {
	// 	$hide_active = "AND project.active = 'true' AND category.active = 'true'";
	// }

	include "inc/header-html.php";

// PHP 5 antiquated
	// mysql_select_db($database_nh_com_db, $nh_com_db);
	$query_projects_home = "SELECT *,
	category.title AS category_title, 
	project.id AS project_id,
	project.title AS project_title, 
	project.path AS project_path, 
	category.path AS category_path 
	FROM project 
	LEFT JOIN category ON category.id=project.category_id 
	WHERE category.id != '99' ".$hide_active." 
	ORDER BY category.`order`, project.`order` ASC";


// PHP 5 antiquated
	// $projects_home = mysql_query($query_projects_home, $nh_com_db) or die(mysql_error());
	// $row_projects_home = mysql_fetch_assoc($projects_home);
	// $totalRows_projects_home = mysql_num_rows($projects_home);


	// PHP 7
	$projects_home = mysqli_query($nh_com_db, $query_projects_home) or die(mysqli_connect_error());
	$row_projects_home = mysqli_fetch_assoc($projects_home);
	$totalRows_projects_home = mysqli_num_rows($projects_home);




?>
	</head>
	<body class="p-<?=$page_name?>">
		<? include "inc/header.php"; ?>
		<div class="container hd main">
<?
	$category_last = NULL;
	$category_count = 0;
	$category_count = 0;
	$number_of_columns = 6;
	function new_section_start() { 
?>
			<section>
<?	}
	function new_headline($category_title) { ?>
				<div class="row">
					<div class="columns twelve">
						<h2><?=$category_title?></h2>
					</div><!-- /.columns (start new()) -->
				</div><!-- /.row -->
<?	}
	function new_thumb_row_start($insert_class) {  
?>
				<div class="row">
					<ul class="thumbnails-default <?=$insert_class?>">
<?	}
	function new_headline_and_thumb_row_end() {  
?>

					</ul><!-- /.thumbnails-default -->
				</div><!-- /.row 1 -->
			</section><!-- /.section 2 -->
<?	} 

	do { 
		if ($row_projects_home['project_id'] !== '14') {
			$project_path = "/".$row_projects_home['project_path']."/";
		} else {
			$project_path = "http://3d.nathanhiemstra.com/";
		}

		$thumbnail_extension = 'jpg';
		if ($row_projects_home['project_path'] == 'animated-gifs' || $row_projects_home['project_path'] == 'animate-found-photos') {
			$thumbnail_extension = 'gif';
		} else if ($row_projects_home['project_path'] == 'phonebook-puns' || $row_projects_home['project_path'] == 'circus-wagon-wheels' || $row_projects_home['project_path'] == 'band-and-the-names' | $row_projects_home['project_path'] == 'disliked-food-descriptions') {
			$thumbnail_extension = 'png';
		}

		

		// If it's  a new category, close off the last and open a new one.
		if ($row_projects_home['category_id'] !== $category_last) {
			// If it's any but the first category, close the previous category
			if ($category_count > 0) {
				new_headline_and_thumb_row_end();
			} // end if 
 			new_section_start();
	 		new_headline($row_projects_home['category_title']);
			new_thumb_row_start("");
			$column_count = 0;
		} // end if 


		// If it's  a new category, close off the last and open a new one.
		if ($column_count == $number_of_columns) {
 			new_headline_and_thumb_row_end();
 			new_section_start();
	 		new_thumb_row_start("not-first-row");
	 		$column_count = 0;
		} // end if 
?>
						<li class="columns two thumb-wrapper">
							<a href="<?=$project_path?>">
								<img 
									src="/_lib/images/home/thumbnails/<?=$row_projects_home['project_path']?>.<?=$thumbnail_extension?>" 
									alt="<?=$row_projects_home['project_title']?> thumbnail"
									class="<?=$row_projects_home['project_path']?>-thumbnail"
								>
							</a>
							<a href="<?=$project_path?>">
								<?=$row_projects_home['project_title']?>
							</a>
						</li><!-- /.columns <li> loop -->
			<? 
				$category_last = $row_projects_home['category_id'];
				$category_count++; 
				$column_count++;
			?>
		<?php } while ($row_projects_home = mysqli_fetch_assoc($projects_home)); ?>
	
				</ul><!-- /.thumbnails-default -->
			</div><!-- /.row -->
		</section><!-- /.section -->
	</div><!-- /.container -->

<?php include "inc/footer.php"; ?>
