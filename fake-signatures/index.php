

<?
	// variables
	$title = "Fake Signatures";
	$section_name = "collections";
	$project_name = "fake-signatures";
	$page_name = "fake-signatures";
	$cur = 1;
	$description = "When merchant's don't ask for my ID when use my bank card, I sign any name I want.";

	include "../inc/header-html.php";

	$query_fake_signatures = "SELECT * FROM fake_signatures ORDER BY fake_signatures.name";
	
	// PHP 5 antiquated
	// $fake_signatures = mysql_query($query_fake_signatures, $nh_com_db) or die(mysql_error());
	// $row_fake_signatures = mysql_fetch_assoc($fake_signatures);
	// $totalRows_fake_signatures = mysql_num_rows($fake_signatures);


	// PHP 7 
	$fake_signatures = mysqli_query($nh_com_db, $query_fake_signatures) or die(mysqli_connect_error());
	$row_fake_signatures = mysqli_fetch_assoc($fake_signatures);
	$totalRows_fake_signatures = mysqli_num_rows($fake_signatures);

	include "../_lib/functions/build-thumbnails.php"; 
?>

</head>
<body class="p-<?=$page_name?>">
	<? include "../inc/header.php"; ?>
	<div class="row">
		<header role="banner">
			<div class="container hd clearfix">
				<h1><?=$title?></h1>	
			</div><!-- end .container -->

		</header><!-- end #hd -->
	</div>
	<div class="container hd" class="main">
		<section>			
			<div class="row">

			
				<ul class="thumbnails-default">
					<?php do { ?>
					<?
						$new_row = '</ul>';
						$new_row .= "\r\n\t\t\t\t\t";
						$new_row .= '</div>';
						$new_row .= "\r\n\t\t\t\t\t";
						$new_row .= '<div class="row">';
						$new_row .= "\r\n\t\t\t\t\t\t";
						$new_row .= '<ul class="thumbnails-default">';
						$new_row .= "\r\n\t\t\t\t\t\t\t";	
						if ($cur == 7) {
							echo $new_row;
							$cur = 1;
						} 
						$cur++;		
					?>
					<li class="columns two">
						<a 
							href="/_lib/images/<?=$project_name?>/lg/<?=$row_fake_signatures['filename']?>" 
							title="<?php echo $row_fake_signatures['name']; ?>" 
							class="fancybox-thumbs" 
							data-fancybox-group="thumb" 
							>
								<img src="/_lib/images/<?=$project_name?>/sm/<?=$row_fake_signatures['filename']?>" alt="<?php echo $row_fake_signatures['name']; ?>" />
						</a>
					</li>
					<?php } while ($row_fake_signatures = mysqli_fetch_assoc($fake_signatures)); ?>
			    </ul>
			</div>
		</section>	 
	</div><!-- /. main -->
<? include "../inc/footer.php"; ?>
