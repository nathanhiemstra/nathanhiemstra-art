<?php    
    // Include files 
    include "../inc/header-html.php"; 


    // DB logic
    if (!function_exists("GetSQLValueString")) {
        function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {
          if (PHP_VERSION < 6) {
            $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
          }

          $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

          switch ($theType) {
            case "text":
              $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
              break;    
            case "long":
            case "int":
              $theValue = ($theValue != "") ? intval($theValue) : "NULL";
              break;
            case "double":
              $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
              break;
            case "date":
              $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
              break;
            case "defined":
              $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
              break;
          }
          return $theValue;
        }
    }

    if (!$found_photo_item_animated_id) {
        $found_photo_item_animated_id = "1";
    }

    // DB variables
    mysql_select_db($database_nh_com_db, $nh_com_db);
    $query_found_photo_item_animated = "SELECT *  FROM found_photos_animated WHERE id = '".$found_photo_item_animated_id."'";
    $found_photo_item_animated = mysql_query($query_found_photo_item_animated, $nh_com_db) or die(mysql_error());
    $row_found_photo_item_animated = mysql_fetch_assoc($found_photo_item_animated);
    $totalRows_found_photo_item_animated = mysql_num_rows($found_photo_item_animated);

    // My variables
    $title =  $row_found_photo_item_animated['title'];
    $col_width = $row_found_photo_item_animated['col_width'];
    $filename = $row_found_photo_item_animated['filename'];
    $frame_count_total = $row_found_photo_item_animated['frames'];
    $section_name = "animate-found-photos";
    $project_name = "animate-found-photos";
    $description = "";

    // Initialize Javascript fucntion for image fading and speed
    $initialize_js_funtion = "
        animateFP.util.init({
            containerContainer : '.p-".$page_name."',
            speedFade          : ".$row_found_photo_item_animated['speed_fade'].",
            speedFadeSlowest   : ".$row_found_photo_item_animated['speed_fade_slowest'].",
            speedFadeFastest   : ".$row_found_photo_item_animated['speed_fade_fastest'].",
            speedInterval      : ".$row_found_photo_item_animated['speed_interval']."
        });
    ";
?>
</head>
<body class="p-<?=$page_name?>">
    <? include "../inc/header.php"; ?>
    <div class="row">
        <header role="banner">
            <div class="container hd clearfix">
                <h1><?=$title?></h1>
            </div><!-- end .container -->

        </header><!-- end #hd -->
    </div>

    <div class="container hd" class="main">
        <section>
            <div class="row">
                <div class="columns <?=$col_width?> animate-found-photos">
                    <div class="photos" >
                        <? for ($frame_count = 1; $frame_count <= $frame_count_total; $frame_count++) { ?>
                        <img 
                            src="/_lib/images/animate-found-photos/<?=$filename?>/<?=$frame_count?>.jpg" 
                            alt="<?=$title?> - frame <?=$frame_count?>"  
                        >
                        <? } ?>
                    </div>
                    <div class="controls">
                        <input class="speed-fade" type="range" name="points" min="0" max="5000">
                    </div>
                </div>
            </div>
        </section>
    </div><!-- /. main -->
<?php 
    include "../inc/footer.php";
    mysql_free_result($found_photo_item_animated);
?>