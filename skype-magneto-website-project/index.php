<?
	// variables
	$title = "Skype - Magneto Website Project";
	$section_name = "absurdity-with-strangers";
	$project_name = "absurdity-with-strangers";
	$page_name = "skype-magneto-website-project";
	$description = "";

	include "../inc/header-html.php";
	include "../_lib/functions/build-thumbnails.php"; 
?>
<style type="text/css">
/**/
</style>
</head>
<body class="p-<?=$page_name?>">

	<? include "../inc/header.php"; ?>
	<div class="row">
		<header role="banner">
			<div class="container hd clearfix">
				<h1><?=$title?></h1>	
			</div><!-- end .container -->

		</header><!-- end #hd -->
	</div>
	<div class="container hd" class="main">
		<section>			
			<div class="row">
				<div class="columns eight">
					<img src="../_lib/images/skype-magneto-website-project/skype-magneto-website-project-conversation.jpg" alt="Skype - Magneto Website Project Conversation" class="screenshot-item">
				</div>
			</div>
		</section>
	</div><!-- /. main -->
<?php include "../inc/footer.php"; ?>
