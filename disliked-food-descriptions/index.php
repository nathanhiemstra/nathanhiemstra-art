<?
	// variables
	$title = "Disliked Food Descriptions";
	$section_name = "disliked-food-descriptions";
	$project_name = "disliked-food-descriptions";
	$page_name = "disliked-food-descriptions";
	$description = "";

	include "../inc/header-html.php";
	include "../_lib/functions/build-thumbnails.php"; 
?>
</head>
<body class="p-<?=$page_name?>">

	<? include "../inc/header.php"; ?>
	<div class="row">
		<header role="banner">
			<div class="container hd clearfix">
				<h1><?=$title?></h1>	
			</div><!-- end .container -->

		</header><!-- end #hd -->
	</div>
	<div class="container hd" class="main">
		<section>			
			<div class="row">
				<div class="columns five">
					<dl class="desc-list">
						
						<dt class="food anchovies">Anchovies</dt>
						<dd class="desc">Vagina of the Sea </dd>
						<dd class="context">- Sarah Holtschlag</dd>

						<dt class="food beer">Beer</dt>
						<dd class="desc">Earwax</dd>
						<dd class="context">- Nathan Hiemstra</dd>


						<dt class="food beets">Beets</dt>
						<dd class="desc">Innards found in a swamp</dd> 
						<dd class="context">- Anne Hensley</dd>

						<dd class="desc">Basement dirt</dd>
						<dd class="context">- Nathan Hiemstra</dd>

						<dt class="food boiled-vegetables">Boiled vegetables</dt>
						<dd class="desc">Soggy cardboard fiber</dd> 
						<dd class="context">- Aaron Schimer</dd>

						<dt class="food brie">Brie</dt>
						<dd class="desc">The dank caves of southern France</dd> 
						<dd class="context">- Matt S.</dd>

						<dt class="food brussel-sprouts">Brussel Sprouts</dt>
						<dd class="desc">Sour, bitterness. A bad attitude.</dd> 
						<dd class="context">- Andy Farrell </dd>

						<dt class="food cilantro">Celery</dt>
						<dd class="desc">The packing tape of food</dd> 
						<dd class="context">- Nathan Hiemstra</dd>

						<dt class="food cilantro">Cilantro</dt>
						<dd class="desc">Peed-on weed trimmings</dd> 
						<dd class="context">- Derek Lee Barton</dd>

						<dt class="food coffee">Coffee</dt>
						<dd class="desc">Burnt dirt</dd>
						<dd class="context">- Nathan Hiemstra</dd>

						<dt class="food cottage-cheese">Cottage Cheese</dt>
						<dd class="desc">Gross foam you see on beaches</dd>
						<dd class="context">- Marina Goldshteyn</dd>

						<dt class="food cucumber">Cucumbers</dt>
						<dd class="desc">Crunchy water</dd> 
						<dd class="context">- Nathan G.</dd>

						<dt class="food good-n-plentys">Good-N-Plentys</dt>
						<dd class="desc">I swallow them whole so I don't taste them</dd> 
						<dd class="context">- Bryan Dunk</dd>


						<dt class="food hot-dogs">Hot dogs</dt>
						<dd class="desc">Regurgitation of the worst kinds of meat.</dd> 
						<dd class="context">- Kate S.</dd>

						<dt class="food raisins">Hot raisins</dd> 
						<dd class="desc">The ticks of food.</dd> 
						<dd class="context">- Jessica Lynam</dd>

						<dt class="food malort">Malört </dt>
						<dd class="desc">Grapefruit and ashtray</dd> 
						<dd class="context">- Someone at the bar</dd>

						<dd class="desc">The night dad left</dd> 
						<dd class="context">- Someone at the bar</dd>

						
					</dl>
				</div>
				<div class="columns five offset-one">
					<dl class="desc-list">
						

						<dt class="food mushrooms">Mushrooms</dt>
						<dd class="desc">Soggy feet</dd> 
						<dd class="context">- Jim Krok</dd>

						<dt class="food mussels">Mussels</dt>
						<dd class="desc">Salty rubber bands</dd> 
						<dd class="context">- Bobby LaPorte</dd>

						<dt class="food olives">Olives</dt>
						<dd class="desc">B.O. + old leather couch</dd>
						<dd class="context">- Nathan Hiemstra</dd>

						<dt class="food peas">Peas</dt>
						<dd class="desc">Pimples</dd> 
						<dd class="context">- Anna Coyle's Dad</dd>

						<dt class="food pizza">Pizza</dt>
						<dd class="desc">Soggy, mushy, moist</dd> 
						<dd class="context">- Julie Goldberg Gordon </dd>

						<dt class="food potatoes">Potatoes</dt>
						<dd class="desc">Hard to chew, no pay off</dd> 
						<dd class="context">- Mindy Park</dd>

						<dt class="food raisins">Raisins</dt>
						<dd class="desc">Little bits of Hell</dd> 
						<dd class="context">- Anna Coyle's friend</dd>

						<dt class="food onions">Raw onions</dt>
						<dd class="desc">Crunchy beads of armpit sweat</dd> 
						<dd class="context">- May BaMaung </dd>

						<dt class="food seafood">Seafood</dt>
						<dd class="desc">Chewy, slimy, suspicious</dd> 
						<dd class="context">- Jordan V.</dd>

						<dt class="food sour-cream">Sour cream</dt>
						<dd class="desc">Rotten garbage milk </dd>
						<dd class="context">- Mark W.</dd>

						<dt class="food steak">Steak</dt>
						<dd class="desc">Chewing purgatory</dd>
						<dd class="context">- Nathan Hiemstra</dd>


						<dt class="food ranch"></dd>Ranch dressing</dt>
						<dd class="desc">Spoiled milk left outside in an an herb garden and blended together then used as sunscreen on a gross 
							sweaty man </dd>
						<dd class="context">- Marina Goldshteyn</dd>

						

						<dt class="food watermelon">Watermelon</dt>
						<dd class="desc">Soggy pink sponge infested with black beetles that break your teeth or choke you</dd> 
						<dd class="context">- Derek Lee Barton</dd>

						<dt class="food">White pepper</dt>
						<dd class="desc">Tastes like the smell of cow manure</dd> 
						<dd class="context">- Amy Boucher</dd>
					</dl>
				</div>
			</div>
		
			
		</section>
	</div><!-- /. main -->
<?php include "../inc/footer.php"; ?>