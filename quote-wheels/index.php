<?
	// variables
	$title = "Quote Wheels";
	$section_name = "social-practice";
	$project_name = "quote-wheels";
	$page_name = "quote-wheels";
	$description = "I have been collecting quotes since 1992. These quote wheels are physical devices I made for these people to view the quotes I got specifically from them.";

	include "../inc/header-html.php";
	include "../_lib/functions/build-thumbnails.php"; 
?>

</head>
<body class="p-<?=$page_name?>">
	<? include "../inc/header.php"; ?>
	<div class="row">
		<header role="banner">
			<div class="container hd clearfix">
				<h1><?=$title?></h1>	
			</div><!-- end .container -->
		</header><!-- end #hd -->
	</div>
	<div class="container hd" class="main">
		<section>			
			<div class="row">
				<ul class="thumbnails-default">
					<li class="columns two">
						<a href="/quote-wheels-terry/">
							<img src="../_lib/images/quote-wheels/sm/terry.jpg" alt="Terry's Quote Wheel"> 
						</a>
					</li>
					<li class="columns two">
						<a href="/quote-wheels-mark/">
							<img src="../_lib/images/quote-wheels/sm/mark.jpg" alt="Mark's Quote Wheel"> 
						</a>
					</li>
					<li class="columns two">
						<a href="/quote-wheels-kirsten/">
							<img src="../_lib/images/quote-wheels/sm/kirsten.jpg" alt="Kirsten's Quote Wheel"> 
						</a>
					</li>
					<li class="columns two">
						<a href="/quote-wheels-geoff/">
							<img src="../_lib/images/quote-wheels/sm/geoff.jpg" alt="Geoff's Quote Wheel"> 
						</a>
					</li>					
					<li class="columns two">
						<a href="/quote-wheels-marina/">
							<img src="../_lib/images/quote-wheels/sm/marina.jpg" alt="Marina's Quote Wheel"> 
						</a>
					</li>
				</ul>
			</div>
			<div class="row">
					<div class="columns ten">
						<p>I’ve been collecting overheard-quotes and my own amateur adages since 1992. They have become an unexpected biography, documenting the people and phases in my life and how I’ve evolved.</p>

						<p>These quote wheels are physical objects printed on card stock with discs that can be manually turned, sliding subsequent quotes into the speech bubble window. Each disc has its own window cut out that lines up with the speech bubble windo, allowing quotes from underlying disc be visible. Magnets help lock these windows into alignment.</p>

						<p>The quote wheels are gifts I’ve made for friends containing the quotes I collected from them over the years. They make me feel closer to these friends because 1) I’m reminded of things I love about them and 2) not only do they say “I care”, but “I’m listening”.</p>

						
					</div>
				</div>
		</section>
	</div><!-- /. main -->
<?php include "../inc/footer.php"; ?>
