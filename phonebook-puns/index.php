<?
	// variables
	$title = "Phonebook Puns";
	$section_name = "collections";
	$project_name = "phonebook-puns";
	$page_name = "phonebook-puns";
	$description = "Pun names found in the phone book.";

	include "../inc/header-html.php";
	include "../_lib/functions/build-thumbnails.php"; 
?>

</head>
<body class="p-<?=$page_name?>">
	<? include "../inc/header.php"; ?>
	<div class="row">
		<header role="banner">
			<div class="container hd clearfix">
				<h1><?=$title?></h1>	
			</div><!-- end .container -->

		</header><!-- end #hd -->
	</div>
	<div class="container hd" class="main">
		<section>			
			<div class="row">
				<? /*
				<ul class="thumbnails-default">
					<?php
						buildThumbnails($project_name);
					?>
				</ul>
				*/ ?>
				<ul class="thumbnails-default">	 
					<li class="columns two">
						<a 
							href="../_lib/images/phonebook-puns/lg/afford.png" 
							class="fancybox-effects-c" 
							data-fancybox-group="thumb"
							title="Afford">
								<img src="../_lib/images/phonebook-puns/sm/afford.png" alt="afford.png"> 
						</a>
					</li> 
					<li class="columns two">
						<a 
							href="../_lib/images/phonebook-puns/lg/chicago-blairs.png"
							class="fancybox-effects-c" 
							data-fancybox-group="thumb"
							title="The Chicago Blairs">
								<img src="../_lib/images/phonebook-puns/sm/chicago-blairs.png" alt="chicago-blairs.png"> 
						</a>
					</li> 
					<li class="columns two">
						<a 
							href="../_lib/images/phonebook-puns/lg/detroit-ryans.png"
							class="fancybox-effects-c" 
							data-fancybox-group="thumb"
							title="The Detroit Ryans">
								<img src="../_lib/images/phonebook-puns/sm/detroit-ryans.png" alt="detroit-ryans.png"> 
						</a>
					</li> 
					<li class="columns two">
						<a 
							href="../_lib/images/phonebook-puns/lg/joe-mama.png"
							class="fancybox-effects-c" 
							data-fancybox-group="thumb"
							title="Joe Mama">
								<img src="../_lib/images/phonebook-puns/sm/joe-mama.png" alt="joe-mama.png"> 
						</a>
					</li> 
					<li class="columns two">
						<a 
							href="../_lib/images/phonebook-puns/lg/num-chuck.png"
							class="fancybox-effects-c" 
							data-fancybox-group="thumb"
							title="Num Chuck">
								<img src="../_lib/images/phonebook-puns/sm/num-chuck.png" alt="num-chuck.png"> 
						</a>
					</li> 
					<li class="columns two">
						<a 
							href="../_lib/images/phonebook-puns/lg/warren-peace.png"
							class="fancybox-effects-c" 
							data-fancybox-group="thumb"
							title="Warren Peace">
								<img src="../_lib/images/phonebook-puns/sm/warren-peace.png" alt="warren-peace.png"> 
						</a>
					</li>				
				</ul>
			</div>
		</section>
	</div><!-- /. main -->
<?php include "../inc/footer.php"; ?>
