<? 
// Variables 
$fb_image = NULL;

/* ?>
<?  if ($page_name != 'home') { ?>
<section>           
    <div class="row">
        <div class="container hd columns twelve">
            <div id="disqus_thread"></div>
        </div>
    </div>
</section>
<? } ?>
<? */ ?>


<hr class="nav-divider">

<div class="row">
	<div class="container hd clearfix footer">
		Copyright &copy; 1997-<?=date('Y')?>  <a href="mailto:mail@nathanhiemstra.com">Nathan Hiemstra</a> &mdash; Artist, Chicago, IL.
	</div>
</div>

<!-- AddThis Smart Layers BEGIN -->
<!-- Go to http://www.addthis.com/get/smart-layers to customize -->

<!-- AddThis Smart Layers END -->
<meta property="og:image" content="http://nathanhiemstra.com/_lib/images/<?=$fb_image?>"> 

<script type="text/javascript" src="/_lib/scripts/base.js"></script>

<? 
  if (isset($initialize_js_funtion)) {
      echo '<script>
    $(document).ready(function() {
      '.$initialize_js_funtion.'
    });
  </script>';
  }
  ?>

<!-- Add fancyBox main JS -->
<script type="text/javascript" src="/_lib/scripts/fancyBox/source/jquery.fancybox.js?v=2.1.5"></script>


<? /*
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="/_lib/scripts/fancyBox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

<!-- Add Button helper (this is optional) -->
<link rel="stylesheet" type="text/css" href="/_lib/scripts/fancyBox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
<script type="text/javascript" src="/_lib/scripts/fancyBox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>

<!-- Add Thumbnail helper (this is optional) -->
<link rel="stylesheet" type="text/css" href="/_lib/scripts/fancyBox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
<script type="text/javascript" src="/_lib/scripts/fancyBox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

<!-- Add Media helper (this is optional) -->
<script type="text/javascript" src="/_lib/scripts/fancyBox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

<!--
<script type="text/javascript" src="/_lib/scripts/disqus.js"></script>
-->
*/ ?>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-3469091-4', 'nathanhiemstra.com');
  ga('send', 'pageview');

</script>

</body>
</html>