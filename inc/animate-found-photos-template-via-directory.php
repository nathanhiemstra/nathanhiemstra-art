
<?
    if (!$col_width) {
        $col_width = 'seven';
    } 
    $section_name = "animate-found-photos";
    include "../inc/header-html.php";
    include "../_lib/functions/get-images-in-directory.php";
?>

</head>
<body class="p-<?=$page_name?>">
    <? include "../inc/header.php"; ?>
    <div class="row">
        <header role="banner">
            <div class="container hd clearfix">
                <h1><?=$title?></h1>
            </div><!-- end .container -->

        </header><!-- end #hd -->
    </div>

    <div class="container hd" class="main">
        <section>
            <div class="row">
                <div class="columns <?=$col_width?> animate-found-photos">
                    <div class="photos" >
                        <?php
                            getImagesInDirectory('animate-found-photos/'.$page_name_partial);
                        ?>
                    </div>
                    <div class="controls">
                        <input class="speed-fade" type="range" name="points" min="0" max="5000">
                    </div>
                </div>
            </div>
        </section>
    </div><!-- /. main -->
<?php include "../inc/footer.php"; ?>