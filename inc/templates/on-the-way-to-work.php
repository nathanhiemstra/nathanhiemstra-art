<?php    
    // Include files 
    include "../inc/header-html.php"; 

    // DB logic
    if (!function_exists("GetSQLValueString")) {
        function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {
          if (PHP_VERSION < 6) {
            $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
          }

          $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

          switch ($theType) {
            case "text":
              $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
              break;    
            case "long":
            case "int":
              $theValue = ($theValue != "") ? intval($theValue) : "NULL";
              break;
            case "double":
              $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
              break;
            case "date":
              $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
              break;
            case "defined":
              $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
              break;
          }
          return $theValue;
        }
    }

    if (!$on_the_way_to_work_item_animated_id) {
        $on_the_way_to_work_item_animated_id = "1";
    }

    // DB variables
    mysql_select_db($database_nh_com_db, $nh_com_db);
    $query_on_the_way_to_work_item_animated = "SELECT *  FROM on_the_way_to_work_animated WHERE id = '".$on_the_way_to_work_item_animated_id."'";
    $on_the_way_to_work_item_animated = mysql_query($query_on_the_way_to_work_item_animated, $nh_com_db) or die(mysql_error());
    $row_on_the_way_to_work_item_animated = mysql_fetch_assoc($on_the_way_to_work_item_animated);
    $totalRows_on_the_way_to_work_item_animated = mysql_num_rows($on_the_way_to_work_item_animated);

    $query_on_the_way_to_work_animated = "SELECT * FROM on_the_way_to_work_animated WHERE on_the_way_to_work_animated.active = '1' ORDER BY on_the_way_to_work_animated.order DESC, on_the_way_to_work_animated.id DESC ";
    $on_the_way_to_work_animated = mysql_query($query_on_the_way_to_work_animated, $nh_com_db) or die(mysql_error());
    $row_on_the_way_to_work_animated = mysql_fetch_assoc($on_the_way_to_work_animated);
    $totalRows_on_the_way_to_work_animated = mysql_num_rows($on_the_way_to_work_animated);

    // My variables
    $this_id =  $row_on_the_way_to_work_item_animated['id'];
    $title =  $row_on_the_way_to_work_item_animated['title'];
    $col_width = $row_on_the_way_to_work_item_animated['col_width'];
    $filename = $row_on_the_way_to_work_item_animated['filename'];
    $frame_count_total = $row_on_the_way_to_work_item_animated['frames'];
    $section_name = "on-the-way-to-work";
    $project_name = "on-the-way-to-work";
    $description = "";

    // Initialize Javascript fucntion for image fading and speed
    $initialize_js_funtion = "
        animateFP.util.init({
            containerContainer : '.p-".$page_name."',
            speedFade          : ".$row_on_the_way_to_work_item_animated['speed_fade'].",
            speedFadeSlowest   : ".$row_on_the_way_to_work_item_animated['speed_fade_slowest'].",
            speedFadeFastest   : ".$row_on_the_way_to_work_item_animated['speed_fade_fastest'].",
            speedInterval      : ".$row_on_the_way_to_work_item_animated['speed_interval']."
        });
        nextPrevViaArrowKeys();
    ";

    // NEXT / PREV LINKS
    // Variables
    $last_url = "to_be_determined"; 
    $next_url = "to_be_determined"; 
    $match_found = false;
    $loop_once_more = false;
    $very_first_item = "to_be_determined";

    // Loop
    do { 
        $this_url = "/on-the-way-to-work-".$row_on_the_way_to_work_animated['filename'];

        // Ooo, we just had a match. This will be the "next" one,
        if ($loop_once_more == true) {
            $next_url = $this_url;
            $loop_once_more = false;
        }
        // A match has been found. Tell it to loop once more to get the "next" item.
        if ($match_found == false) {
            if ($row_on_the_way_to_work_animated['id'] == $this_id) {
                 $loop_once_more = true;
                 $match_found = true;

                 // Record what the "prev" link wil be
                 $prev_url = $last_url;
            } 
        } 

        // Remember incase we're on the very last item and we need to return to the first one
        if ($very_first_item == "to_be_determined") {
            $very_first_item = $this_url;
        }

        // Remember for next loop
        $last_url = $this_url;
   
     } while ($row_on_the_way_to_work_animated = mysql_fetch_assoc($on_the_way_to_work_animated)); 
    
    // If it was never definied, that means we're on the very first or last one
    if ($prev_url == "to_be_determined") {
        $prev_url = $last_url;
    }
    if ($next_url == "to_be_determined") {
        $next_url = $very_first_item;
    }
?>
</head>
<body class="p-<?=$page_name?>">
    <? include "../inc/header.php"; ?>
    <div class="row">
        <header role="banner">
            <div class="container hd clearfix">
                <h1><?=$title?></h1>
            </div><!-- end .container -->

        </header><!-- end #hd -->
    </div>

    <div class="container hd" class="main">
        <section>
            <div class="row">
                <div class="columns <?=$col_width?> on-the-way-to-work animate-found-photos">
                    <div class="photos" >
                        <? for ($frame_count = 1; $frame_count <= $frame_count_total; $frame_count++) { ?>
                        <img 
                            src="/_lib/images/on-the-way-to-work/<?=$filename?>/<?=str_pad($frame_count, 2, '0', STR_PAD_LEFT)?>.jpg" 
                            alt="<?=$title?> - frame <?=$frame_count?>"  
                        >
                        <? } ?>
                    </div>
                    <div class="controls">
                        <input class="speed-fade" type="range" name="points" min="0" max="5000">
                    </div>
                    <div class="nav-pagination clearfix">
                        <a href="<?=$prev_url?>" id="nav-page-prev">Prev</a>
                        <a href="<?=$next_url?>" id="nav-page-next" class="right">Next</a>
                    </div>
                </div>
            </div>

        </section>
    </div><!-- /. main -->
<?php 
    include "../inc/footer.php";
    mysql_free_result($on_the_way_to_work_item_animated);
?>