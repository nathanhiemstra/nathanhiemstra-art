<?
	

	// Projects
	$query_projects = "SELECT *,
						category.id AS category_id,
						category.title AS category_title,
						project.id AS project_id, 
						project.title AS project_title, 
						project.path AS project_path, 
						category.path AS category_path,
						category.default_project_id AS category_default_project_id
						FROM project 
						LEFT JOIN category ON category.id=project.category_id 
						WHERE project.active = 'true' AND category.active = 'true' 
						ORDER BY category.`order`, project.`order` ASC";
	$projects = mysqli_query($nh_com_db, $query_projects) or die(mysqli_connect_error());
	$row_projects = mysqli_fetch_assoc($projects);
	$totalRows_projects = mysqli_num_rows($projects);

	// Default Project Paths
	$query_project_default = "SELECT *,
						category.id AS category_id, 
						project.path AS project_path
						FROM project,category
						-- LEFT JOIN category ON category.default_project_id=project.id 
						WHERE project.id = category.default_project_id 
						ORDER BY category.`order`, project.`order` ASC";
	$project_default = mysqli_query($nh_com_db, $query_project_default) or die(mysqli_connect_error());
	$row_project_default = mysqli_fetch_assoc($project_default);
	$totalRows_project_default = mysqli_num_rows($project_default);

	// Make array of category IDs and default project paths
	$default_project_path_array[] = "";
	do { 
		$default_project_path_array[$row_project_default['category_id']] = $row_project_default['project_path'];
	} while ($row_project_default = mysqli_fetch_assoc($project_default)); 
?>
			<nav>
				<ul class="nav-main">
<? 
	$category_last = NULL;
	$category_count = 0;

	do { 
		$default_project_path = $default_project_path_array[$row_projects['category_id']];
		if ($row_projects['project_id'] !== '14') {
			$project_path = "/".$row_projects['project_path']."/";
		} else {
			$project_path = "http://3d.nathanhiemstra.com/";
		}
				// If it's  a new category, close off the last <ul> and open a new one.
		if ($row_projects['category_id'] !== $category_last) {
			// If it's any but the first category, close the previous category
			if ($category_count > 0) {
				print '</ul>
				</li>';
			}
			// Start the new Category <li>
?>
			<li class="<?=checkIsSelected($default_project_path,$page_name)?>">
<?
					print '<a href="/'.$default_project_path.'/">'.$row_projects['category_title'].'</a>
					<ul class="sub">';
		}	
?>
					<li class="nav-thumb-<?=$row_projects['project_path']?><? checkIsSelected($row_projects["project_path"],$page_name); ?>">
						
						<a href="<?=$project_path?>">
							<?=$row_projects['project_title']?>
						</a>
					</li>
<? 
		$category_last = $row_projects['category_id'];
		$category_count++; 
	} while ($row_projects = mysqli_fetch_assoc($projects)); 
?>
						</ul>
					</li>
				</ul>
				<? /*
				<ul class="nav-utility">
					<li class="<? checkIsSelected("contact",$page_name); ?>"><a href="mail@nathanhiemstra.com">Contact</a></li>
					<li class="<? checkIsSelected("about",$page_name); ?>"><a href="/about/">About</a></li>
					<li class="<? checkIsSelected("home",$page_name); ?>"><a href="/">Home</a></li>
				</ul>
				*/ ?>
			</nav>
<?php
mysqli_free_result($projects);
mysqli_free_result($project_default);
?>