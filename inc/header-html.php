<?
	// Variables
	$description = NULL;
	$title = NULL;
?>
<? include "".$_SERVER["DOCUMENT_ROOT"]."/_lib/functions/build-facebook-image-tag.php"; ?>
<? include "".$_SERVER["DOCUMENT_ROOT"]."/_lib/functions/check-is-selected.php"; ?>


<? require_once("".$_SERVER["DOCUMENT_ROOT"]."/Connections/nh_com_db.php"); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {
	  if (PHP_VERSION < 6) {
	    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
	  }

	  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

	  switch ($theType) {
	    case "text":
	      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
	      break;    
	    case "long":
	    case "int":
	      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
	      break;
	    case "double":
	      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
	      break;
	    case "date":
	      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
	      break;
	    case "defined":
	      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
	      break;
	  }
	  return $theValue;
	}
}
// mysql_select_db($database_nh_com_db, $nh_com_db);
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>	<html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>	<html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><? if ($title) {echo $title;} else { print "Nathan Hiemstra - Artist";} ?></title>
	<meta name="description" content="<? if ($description) {echo $description;} else { print "Artwork, Chicago, IL";} ?>">
	<meta name="viewport" content="width=device-width,initial-scale=1">

	<link href="/_lib/styles/styles.css" rel="stylesheet" type="text/css" />
	<script src="/_lib/scripts/modernizr.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script>
		if (!window.jQuery) { document.write('<script src="/_lib/scripts/jquery-2.1.4.min.js"><\/script>'); }
	</script>
	<script src="/_lib/scripts/blazy.min.js"></script>
	<script>
      ;(function() {
          // Initialize
          var bLazy = new Blazy();
      })();
  </script>
  
  <!-- Hotjar Tracking Code for http://nathanhiemstra.com -->
	<script>
	    (function(h,o,t,j,a,r){
	        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
	        h._hjSettings={hjid:605200,hjsv:5};
	        a=o.getElementsByTagName('head')[0];
	        r=o.createElement('script');r.async=1;
	        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
	        a.appendChild(r);
	    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
	</script>


	<? /*
	<script src="/_lib/scripts/1.10.1/jquery.min.js"></script>
	*/ ?>
	<!-- Facebook tags -->
    <meta property="og:title" content="<?=$title?><? if ($page_name !== "home") { print " - by Nathan Hiemstra";} ?>"> 
    <meta property="og:description" content="<?=$description?>"> 
    <? // buildFacebookImageTag($project_name,$thumb_size); ?>


