<?php    
    // Include files 
    include "../inc/header-html.php"; 

    // DB logic
    if (!function_exists("GetSQLValueString")) {
        function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {
          if (PHP_VERSION < 6) {
            $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
          }

          $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

          switch ($theType) {
            case "text":
              $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
              break;    
            case "long":
            case "int":
              $theValue = ($theValue != "") ? intval($theValue) : "NULL";
              break;
            case "double":
              $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
              break;
            case "date":
              $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
              break;
            case "defined":
              $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
              break;
          }
          return $theValue;
        }
    }

    if (!$found_photo_item_animated_id) {
        $found_photo_item_animated_id = "1";
    }

    // DB variables
    mysql_select_db($database_nh_com_db, $nh_com_db);
    $query_found_photo_item_animated = "SELECT *  FROM found_photos_animated WHERE id = '".$found_photo_item_animated_id."'";
    $found_photo_item_animated = mysql_query($query_found_photo_item_animated, $nh_com_db) or die(mysql_error());
    $row_found_photo_item_animated = mysql_fetch_assoc($found_photo_item_animated);
    $totalRows_found_photo_item_animated = mysql_num_rows($found_photo_item_animated);

    $query_found_photos_animated = "SELECT * FROM found_photos_animated WHERE found_photos_animated.active = '1' ORDER BY found_photos_animated.order DESC, found_photos_animated.id DESC ";
    $found_photos_animated = mysql_query($query_found_photos_animated, $nh_com_db) or die(mysql_error());
    $row_found_photos_animated = mysql_fetch_assoc($found_photos_animated);
    $totalRows_found_photos_animated = mysql_num_rows($found_photos_animated);

    // My variables
    $this_id =  $row_found_photo_item_animated['id'];
    $title =  $row_found_photo_item_animated['title'];
    if ( $row_found_photo_item_animated['vertical']) { $is_vertical = 'vertical'; }
    $filename = $row_found_photo_item_animated['filename'];
    $frame_count_total = $row_found_photo_item_animated['frames'];
    $section_name = "animate-found-photos";
    $project_name = "animate-found-photos";
    $description = "";

    // Initialize Javascript fucntion for image fading and speed
    $initialize_js_funtion = "
        animateFP.util.init({
            containerContainer : '.p-".$page_name."',
            speedFade          : ".$row_found_photo_item_animated['speed_fade'].",
            speedFadeSlowest   : ".$row_found_photo_item_animated['speed_fade_slowest'].",
            speedFadeFastest   : ".$row_found_photo_item_animated['speed_fade_fastest'].",
            speedInterval      : ".$row_found_photo_item_animated['speed_interval']."
        });
        nextPrevViaArrowKeys();
    ";

    // NEXT / PREV LINKS
    // Variables
    $last_url = "to_be_determined"; 
    $next_url = "to_be_determined"; 
    $match_found = false;
    $loop_once_more = false;
    $very_first_item = "to_be_determined";

    // Loop
    do { 
        $this_url = "/animate-found-photos-".$row_found_photos_animated['filename'];

        // Ooo, we just had a match. This will be the "next" one,
        if ($loop_once_more == true) {
            $next_url = $this_url;
            $loop_once_more = false;
        }
        // A match has been found. Tell it to loop once more to get the "next" item.
        if ($match_found == false) {
            if ($row_found_photos_animated['id'] == $this_id) {
                 $loop_once_more = true;
                 $match_found = true;

                 // Record what the "prev" link wil be
                 $prev_url = $last_url;
            } 
        } 

        // Remember incase we're on the very last item and we need to return to the first one
        if ($very_first_item == "to_be_determined") {
            $very_first_item = $this_url;
        }

        // Remember for next loop
        $last_url = $this_url;
   
     } while ($row_found_photos_animated = mysql_fetch_assoc($found_photos_animated)); 
    
    // If it was never definied, that means we're on the very first or last one
    if ($prev_url == "to_be_determined") {
        $prev_url = $last_url;
    }
    if ($next_url == "to_be_determined") {
        $next_url = $very_first_item;
    }
?>
</head>
<body class="p-<?=$page_name?>">
    <? include "../inc/header.php"; ?>
    <div class="row">
        <header role="banner">
            <div class="container hd clearfix">
                <h1><?=$title?></h1>
            </div><!-- end .container -->

        </header><!-- end #hd -->
    </div>

    <div class="container hd" class="main">
        <section>
            <div class="row">
                <div class="columns twelve animate-found-photos">
                    <div id="photo" class="photos <?=$is_vertical?>" >
                        <? for ($frame_count = 1; $frame_count <= $frame_count_total; $frame_count++) { ?>
                        <img 
                            src="/_lib/images/animate-found-photos/<?=$filename?>/<?=$frame_count?>.jpg" 
                            alt="<?=$title?> - frame <?=$frame_count?>"  
                        >
                        <? } ?>
                    </div>
                    <div class="controls">
                        <input class="speed-fade" type="range" name="points" min="0" max="5000">
                    </div>
                    <div class="nav-pagination clearfix">
                        <a href="<?=$prev_url?>#photo" id="nav-page-prev">Prev</a>
                        <a href="<?=$next_url?>#photo" id="nav-page-next" class="right">Next</a>
                    </div>
                </div>
            </div>

        </section>
    </div><!-- /. main -->
<?php 
    include "../inc/footer.php";
    mysql_free_result($found_photo_item_animated);
?>