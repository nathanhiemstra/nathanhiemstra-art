<?
	// variables
	$title = "Animate Found Photos";
	$section_name = "animate-found-photos";
	$project_name = "animate-found-photos";
	$page_name = "animate-found-photos";
	$cur = 1;
	$description = "I find photos taken moments apart, then animate them.";

	include "../inc/header-html.php";

	$query_found_photos_animated = "SELECT * FROM found_photos_animated WHERE found_photos_animated.active = '1' ORDER BY found_photos_animated.order DESC, found_photos_animated.id DESC ";

	// PHP 5 antiquated
	// $found_photos_animated = mysql_query($query_found_photos_animated, $nh_com_db) or die(mysql_error());
	// $row_found_photos_animated = mysql_fetch_assoc($found_photos_animated);
	// $totalRows_found_photos_animated = mysql_num_rows($found_photos_animated);


	// PHP 7
	$found_photos_animated = mysqli_query($nh_com_db, $query_found_photos_animated) or die(mysqli_connect_error());
	$row_found_photos_animated = mysqli_fetch_assoc($found_photos_animated);
	$totalRows_found_photos_animated = mysqli_num_rows($found_photos_animated);

	include "../_lib/functions/build-thumbnails.php"; 
?>

</head>
<body class="p-<?=$page_name?>">
	<? include "../inc/header.php"; ?>
	<div class="row">
		<header role="banner">
			<div class="container hd clearfix">
				<h1><?=$title?></h1>	
				<!-- <div class="project-description">
					<p>I love old photos. The escapist in me loves fantasizing about these bygone times and people. I can’t imagine wearing their clothes, being in their state of mind, or fitting into their world. </p>
<p>When I look through old photos I search for shots taken moments apart. When I line them up and fade from one to the next, it creates unexpected movement, bringing these static images to life. When I see the movement of their hair in a breeze, or the shift in their bodies, they become real in a way that seems shouldn’t be possible. The fantasy I’m already in envelopes. I feel like I’ve found a secret passage into another world.  </p>

				</div> -->
			</div><!-- end .container -->
		</header><!-- end #hd -->
	</div>
	<div class="container hd" class="main">
		<section>			
			<div class="row">

				<? /*
				<ul class="list">
					<li>
						<a class="various fancybox.ajax" href="/animate-found-photos-ajax/index.php">Ajax</a>
					</li>
				</ul>
				*/ ?>

				<script>
					var bLazy = new Blazy({ 
			    	selector: 'img' // all images
			    });
				</script>

				<ul class="thumbnails-default">

					<?php do { ?>
					<?

						$new_row = '</ul>';
						$new_row .= "\r\n\t\t\t\t\t";
						$new_row .= '</div>';
						$new_row .= "\r\n\t\t\t\t\t";
						$new_row .= '<div class="row">';
						$new_row .= "\r\n\t\t\t\t\t\t";
						$new_row .= '<ul class="thumbnails-default">';
						$new_row .= "\r\n\t\t\t\t\t\t\t";	
						if ($cur == 5) {
							echo $new_row;
							$cur = 1;
						} 
						$cur++;	
							
						if ($row_found_photos_animated['frames']) {
							$frames = $row_found_photos_animated['frames'];
						} else {
							$frames = 2;
						}	
						$file_name = $row_found_photos_animated['filename'];


					?>
					<li class="columns three thumb-wrapper">
						<a 
							href="/animate-found-photos-<?=$file_name?>/" 
							class="cinemagraph-thumb cinemagraph-thumb-<?=$file_name?>"
							frames="<?=$frames?>" 
							>
								<?
								for ($x = 1; $x <= $frames; $x++) {
								    echo '<span 
								    	class="frame frame-'.$x.' b-lazy" 
								    	data-src="/_lib/images/cinemagraphs/'.$frames.'-frames/'.$file_name.'.jpg"></span>';
								} 
								?>
								<?=$row_found_photos_animated['title']?>
						</a>
					</li>
					<?php } while ($row_found_photos_animated = mysqli_fetch_assoc($found_photos_animated)); ?>
				</ul> 
				
			</div>
		</section>
	</div><!-- /. main -->
<?php include "../inc/footer.php"; ?>


