

<?php    
    $page_name = NULL;
    $is_vertical = NULL;
    $zoom_positions = NULL;

    // Include files 
    include "".$_SERVER["DOCUMENT_ROOT"]."/inc/header-html.php"; 



    // DB logic
    if (!function_exists("GetSQLValueString")) {
        function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {
          if (PHP_VERSION < 6) {
            $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
          }

          $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

          switch ($theType) {
            case "text":
              $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
              break;    
            case "long":
            case "int":
              $theValue = ($theValue != "") ? intval($theValue) : "NULL";
              break;
            case "double":
              $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
              break;
            case "date":
              $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
              break;
            case "defined":
              $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
              break;
          }
          return $theValue;
        }
    }


    $urlString= $_SERVER['REQUEST_URI'];
    $arr = explode('/animate-found-photos-', $urlString);
    $selectMethod = NULL;
    if (!empty($arr[1])) {
        $photo_item = $arr[1];
        $found_photo_item_animated_id = rtrim($photo_item, "/");
        $selectMethod = "filename";
    } else {
        //no item-name:
        $found_photo_item_animated_id = "1";
        $selectMethod = "id";
    }



    // DB variables
    // SELECT BY FILENAME
    // PHP 5 antiquated
    // mysql_select_db($database_nh_com_db, $nh_com_db);
    // $query_found_photo_item_animated = "SELECT *  FROM found_photos_animated WHERE ".$selectMethod." = '".$found_photo_item_animated_id."'";
    // $found_photo_item_animated = mysql_query($query_found_photo_item_animated, $nh_com_db) or die(mysql_error());
    // $row_found_photo_item_animated = mysqli_fetch_assoc($found_photo_item_animated);
    // $totalRows_found_photo_item_animated = mysql_num_rows($found_photo_item_animated);

    // PHP 7
    $query_found_photo_item_animated = "SELECT *  FROM found_photos_animated WHERE ".$selectMethod." = '".$found_photo_item_animated_id."'";
    $found_photo_item_animated = mysqli_query($nh_com_db, $query_found_photo_item_animated) or die(mysqli_connect_error());
    $row_found_photo_item_animated = mysqli_fetch_assoc($found_photo_item_animated);
    $totalRows_found_photo_item_animated = mysqli_num_rows($found_photo_item_animated);



    // Get Next/Prev items
    $query_found_photos_animated = "SELECT * FROM found_photos_animated WHERE found_photos_animated.active = '1' ORDER BY found_photos_animated.order DESC, found_photos_animated.id DESC ";


    // PHP 5 antiquated
    // $found_photos_animated = mysql_query($query_found_photos_animated, $nh_com_db) or die(mysql_error());
    // $row_found_photos_animated = mysqli_fetch_assoc($found_photos_animated);
    // $totalRows_found_photos_animated = mysql_num_rows($found_photos_animated);

    // PHP 7
    $found_photos_animated = mysqli_query($nh_com_db, $query_found_photos_animated) or die(mysqli_connect_error());
    $row_found_photos_animated = mysqli_fetch_assoc($found_photos_animated);
    $totalRows_found_photos_animated = mysqli_num_rows($found_photos_animated);

    // My variables
    $this_id =  $row_found_photo_item_animated['id'];
    $title =  $row_found_photo_item_animated['title'];
    if ( $row_found_photo_item_animated['vertical']) { $is_vertical = 'vertical'; }
    $filename = $row_found_photo_item_animated['filename'];
    $frame_count_total = $row_found_photo_item_animated['frames'];
    if ( isset($row_found_photo_item_animated['zoom_positions']) ) {
        $zoom_positions = $row_found_photo_item_animated['zoom_positions'];
    }
    $section_name = "animate-found-photos";
    $project_name = "animate-found-photos";
    $description = "";
    $speed_from_db = $row_found_photo_item_animated['speed'];
    if ( $speed_from_db == 2 ) {
        $speed = "medium";
    } elseif ( $speed_from_db == 3 ) {
        $speed = "slow";
    } else {
        $speed = "fast";
    }

    // Initialize Javascript fucntion for image fading and speed
    $initialize_js_funtion = "
        animateFP.util.init({
            containerContainer : '.p-".$page_name."'
        });
        nextPrevViaArrowKeys();
    ";

    // NEXT / PREV LINKS
    // Variables
    $last_url = "to_be_determined"; 
    $next_url = "to_be_determined"; 
    $match_found = false;
    $loop_once_more = false;
    $very_first_item = "to_be_determined";
    $prev_url = NULL;


    // Loop
    do { 
        $this_url = "/animate-found-photos-".$row_found_photos_animated['filename'];

        // Ooo, we just had a match. This will be the "next" one,
        if ($loop_once_more == true) {
            $next_url = $this_url;
            $loop_once_more = false;
        }
        // A match has been found. Tell it to loop once more to get the "next" item.
        if ($match_found == false) {
            if ($row_found_photos_animated['id'] == $this_id) {
                 $loop_once_more = true;
                 $match_found = true;

                 // Record what the "prev" link wil be
                 $prev_url = $last_url;
            } 
        } 

        // Remember incase we're on the very last item and we need to return to the first one
        if ($very_first_item == "to_be_determined") {
            $very_first_item = $this_url;
        }

        // Remember for next loop
        $last_url = $this_url;
   
     } while ($row_found_photos_animated = mysqli_fetch_assoc($found_photos_animated)); 
    
    // If it was never definied, that means we're on the very first or last one
    if ($prev_url == "to_be_determined") {
        $prev_url = $last_url;
    }
    if ($next_url == "to_be_determined") {
        $next_url = $very_first_item;
    }
?>
</head>
<body class="p-<?=$page_name?>">
    <? include "../inc/header.php"; ?>
    <div class="row">
        <header role="banner">
            <div class="container hd clearfix">
                <h1><?=$title?></h1>
            </div><!-- end .container -->

        </header><!-- end #hd -->
    </div>

    <div class="container hd" class="main">
        <section>
            <div class="row">
                <div class="columns twelve animate-found-photos">
                    <div class="photo-container">
                        <div 
                            id="photo" 
                            class="photos <?=$filename?> <?=$is_vertical?>" 
                            speed="<?=$speed?>" 
                            frames="<?=$frame_count_total?>"
                            <? if ( $zoom_positions ) { ?>zoom-position-current="0" zoom-positions="<?=$zoom_positions?>" <? } ?>
                            >
                            <? for ($frame_count = 1; $frame_count <= $frame_count_total; $frame_count++) { ?>
                            <img 
                                src="/_lib/images/cinemagraphs/<?=$filename?>/<?=$frame_count?>.jpg" 
                                alt="<?=$title?> - frame <?=$frame_count?>" 
                                class="frame frame-<?=$frame_count?>"
                            >
                            <? } ?>
                        </div>
                        <div class="nav-pagination clearfix">
                            <a href="<?=$prev_url?>#photo" id="nav-page-prev">Prev</a>
                            <a href="<?=$next_url?>#photo" id="nav-page-next" class="right">Next</a>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div><!-- /. main -->
<?php 
    include "../inc/footer.php";
    mysqli_free_result($found_photo_item_animated);
?>