<?
	// variables
	$title = "Fake Conversations";
	$section_name = "social-practice";
	$project_name = "fake-conversations";
	$page_name = "fake-conversations";
	$description = "";

	include "../inc/header-html.php";
	include "../_lib/functions/build-thumbnails.php"; 
?>

</head>
<body class="p-<?=$page_name?>">
	<? include "../inc/header.php"; ?>
	<div class="row">
		<header role="banner">
			<div class="container hd clearfix">
				<h1><?=$title?></h1>	
			</div><!-- end .container -->

		</header><!-- end #hd -->
	</div>
	<div class="container hd" class="main">
		<section>			
			<div class="row">
				<ul class="thumbnails-default">
					<li class="columns two">
						<a href="/craigslist-money-found/">
							<img src="../_lib/images/fake-conversations/sm/craigslist-money-found.jpg" alt="craigslist-money-found.jpg"> 
						</a>
					</li>
					<li class="columns two">
						<a href="/craigslist-scanning-job/">
							<img src="../_lib/images/fake-conversations/sm/craigslist-scan-job.jpg" alt="craigslist-scan-job.jpg"> 
						</a>
					</li> 
					<li class="columns two">
						<a href="/skype-magneto-website-project/">
							<img src="../_lib/images/fake-conversations/sm/skype-magneto-website-project.jpg" alt="miami-fiance.jpg"> 
						</a>
					</li> 
					<li class="columns two">
						<a href="/miami-fiance/">
							<img src="../_lib/images/fake-conversations/sm/miami-fiance.jpg" alt="miami-fiance.jpg"> 
						</a>
					</li> 
				</ul>
			</div>
		</section>
	</div><!-- /. main -->
<?php include "../inc/footer.php"; ?>
