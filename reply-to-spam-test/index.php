<?
	// variables
	$title = "RE: Designing/Development";
	$section_name = "Reply To Spam";
	$project_name = "Reply To Spam";
	$page_name = "reply-to-spam";
	$description = "";

	include "../inc/header-html.php";
	include "../_lib/functions/build-thumbnails.php"; 
?>
<style type="text/css">
/**/
</style>
</head>
<body class="p-<?=$page_name?>">

	<? include "../inc/header.php"; ?>
	<div class="row">
		<header role="banner">
			<div class="container hd clearfix">
				<p><?=$section_name?></p>
				<h1><?=$title?></h1>	
			</div><!-- end .container -->

		</header><!-- end #hd -->
	</div>
	<div class="container hd" class="main">
		<section>			
			<div class="row">
				<ul class="list-no-default email-list">
					<li class="item email-item">
						<div class="item-content">
							<div class="email-header clearfix">
								<div class="left">
									<strong><i>On Wednesday, March 25, 2015 12:10 AM, Jaxon Gray wrote:</i></strong>
								</div>
							</div>
							<div class="email-body">
								<p>Hi,</p>
								<p>
								We have bunch of expert developers who can customize and create a site from scratch as per your needs. Our wordpress services use the best host in the internet. We also provide premium themes from various wordpress theme makers and create your site in a affordable price.</p>
								<p>We specialize in lots of wordpress related services that includes:
								Setting up your first professional wordpress blog.</p>
								<p>Moving from BlogSpot to word press.</p>
								<p>Moving from wordpress.com to self hosted word press blog.</p>
								<p>Creating and Customizing Word Press themes.</p>
								<p>Wordpress SEO.</p>
								<p>Wordpress Maintenance.</p>
								<p>Wordpress Consultancy.</p>
								<p>Wordpress integration with social media.</p>
								<p>Fixing hacked web sites and hardening security.</p>
								<p>Integration with email marketing services.</p>
								<p>We also specialize in:
								Graphic design.</p>
								<p>E-commerce websites.</p>
								<p>Web Branding.</p>
								<p>Logo design.</p>
								<p>Business cards.</p>
								<p>Twitter, Facebook, and other social media marketing.</p>
								<p>Web hosting.</p>
								<p>Thanks &amp; Regards,<br>
								Jaxon</p>
							</div>
						</div>
					</li>
					
					<li class="item email-item">
						<div class="item-content">
							<div class="email-header clearfix">
								<div class="left">
									<strong><i>On Monday, June 22, 2015 10:19 AM, Nathan Hiemstra wrote:</i></strong>
								</div>
							</div>
							<div class="email-body">
								<p>Do you pronounce your name like “Jackson” or “Jason”?</p>
								<p>I started spelling my name differently in in 9th grade I changed it from Nate to Nayt because on principle I dislike silent letters I think things should be intuitive for someone to understand with having to know a minimal amount of conteXtual (lol) information like when I was learning to read in school and found out that “ph” made the “f” sound I was upset and couldn’t believe adults whom I already had a low opinion of could create such a flawed system but also I was forming my identity and it was eXhilarating to leave behind such a central convention as the spelling of my own name to underscore a core belief of mine even if it meant difficulty of explaining and correcting  of pronunciation and spelling and ridicule from cynical and older kids from whom I was already experiencing bullying and I needed to prove to myself that I was willing to change anything about myself if it meant becoming the person I wanted to be and not the person my environment would have shaped me to be if I were passive now I don’t really care about the spelling because I know who I am and I have proved to myself I can shape who I am but I like honoring the 9th grade version of myself and I told that to an author once who was writing my name while signing my book and he wrote “9th grade rules”.</p>
								<p>So What’s the next step?</p>
								<p>Nathan</p>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</section>
	</div><!-- /. main -->
<?php include "../inc/footer.php"; ?>
