<?
	// variables
	$title = "Video";
	$section_name = "videos";
	$project_name = "videos";
	$page_name = "videos";
	$description = "Frivolous ideas made slghtly more legitimate by video format.";

	include "../inc/header-html.php";
	include "../_lib/functions/build-thumbnails.php"; 
?>

</head>
<body class="p-<?=$page_name?>">
	<? include "../inc/header.php"; ?>
	<div class="row">
		<header role="banner">
			<div class="container hd clearfix">
				<h1><?=$title?></h1>	
			</div><!-- end .container -->

		</header><!-- end #hd -->
	</div>
	<div class="container hd" class="main">
		<section>			
			<div class="row">
				<ul class="thumbnails-default">
					 
					<li class="columns four">
						<video width="320" height="240" controls>
							<source src="/_lib/videos/echo-talk.mp4" type="video/mp4">
							<source src="/_lib/videos/echo-talk.ogv" type="video/ogg">
							Your browser does not support the video tag.
						</video>
						<p>“Echo Talk” 2008. <br>Doing my favorite thing, legitimizing frivolity. Audio from 1995.
					</li> 
					<li class="columns four">
						<video width="320" height="240" controls>
							<source src="/_lib/videos/zipper.mp4" type="video/mp4">
							<source src="/_lib/videos/zipper.ogg" type="video/ogg">
							
						</video>
						<p>“Zipper” 1998.</p>
					</li> 
					<li class="columns four">
						<video width="432" height="240" controls>
							<source src="/_lib/videos/lucas-hears-my-jokes.mp4" type="video/mp4">
							<source src="/_lib/videos/lucas-hears-my-jokes.ogg" type="video/ogg">
							
						</video>
						<p>“Lucas Hears My Jokes” 2013. <br>I write down any joke that comes to mind. Lucas hates my jokes.</p>
					</li> 			
				</ul>
					

					
				</ul>
			</div>
		</section>
	</div><!-- /. main -->
<?php include "../inc/footer.php"; ?>
