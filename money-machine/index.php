<?
	// variables
	$title = "Money Machine";
	$section_name = "money-machine";
	$project_name = "money-machine";
	$page_name = "money-machine";
	$description = "";

	include "../inc/header-html.php";
	include "../_lib/functions/build-thumbnails.php"; 
?>

</head>
<body class="p-<?=$page_name?>">
	<? include "../inc/header.php"; ?>
	<div class="row">
		<header role="banner">
			<div class="container hd clearfix">
				<h1><?=$title?></h1>	
			</div><!-- end .container -->

		</header><!-- end #hd -->
	</div>
	<div class="container hd" class="main">
		<section>			
			<div class="row">
				<ul class="thumbnails-default">
					<li class="columns eight">
						<div class="embed-container">
							<iframe width="100%" height="315" src="https://www.youtube.com/embed/cS9sIQvOwx8" frameborder="0" allowfullscreen></iframe>
						</div>
					</li>
					<?php
						buildThumbnails($project_name);
					?>
					
				</ul>
			</div>
		</section>
	</div><!-- /. main -->
<?php include "../inc/footer.php"; ?>
