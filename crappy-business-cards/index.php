<?
	// variables
	$title = "Crappy Business Cards - Outsider Graphic Design";
	$section_name = "collections";
	$project_name = "crappy-business-cards";
	$page_name = "crappy-business-cards";
	$description = "A collection of business cards done by non-trained designers";

	include "../inc/header-html.php";
	include "../_lib/functions/build-thumbnails.php"; 
?>

</head>
<body class="p-<?=$page_name?>">
	<? include "../inc/header.php"; ?>
	<div class="row">
		<header role="banner">
			<div class="container hd clearfix">
				<h1><?=$title?></h1>	
			</div><!-- end .container -->

		</header><!-- end #hd -->
	</div>
	<div class="container hd" class="main">
		<section>			
			<div class="row">
				<ul class="thumbnails-default">
					<?
						buildThumbnails($project_name);
					?>
				</ul>
			</div>
		</section>
	</div><!-- /. main -->
<? include "../inc/footer.php"; ?>
