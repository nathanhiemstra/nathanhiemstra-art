<?
	// variables
	$title = "Photoshop Challenges";
	$section_name = "photographs";
	$project_name = "photoshop-challenges";
	$page_name = "photoshop-challenges";
	$description = "Photoshopped images to satisfy challenges friends, funny ideas that popped into my head, or images my imagination saw in objects that I then added.";

	include "../inc/header-html.php";
	include "../_lib/functions/build-thumbnails.php";
?>

</head>
<body class="p-<?=$page_name?>">
	<? include "../inc/header.php"; ?>
	<div class="row">
		<header role="banner">
			<div class="container hd clearfix">
				<h1><?=$title?></h1>
			</div><!-- end .container -->

		</header><!-- end #hd -->
	</div>
	<div class="container hd" class="main">
		<section>
			<div class="row">
				<ul class="thumbnails-default">
					<li class="columns two">
						<a href="../_lib/images/photoshop-challenges/lg/frodo-kahlo-1.jpg" class="fancybox-effects-c" data-fancybox-group="thumb" title="“Frodo Kalho” - Challenge by Nate Geisler">
							<img src="../_lib/images/photoshop-challenges/sm/frodo-kahlo-1.jpg" alt="frodo-kahlo-1.jpg">
						</a>
					</li>
					<li class="columns two">
						<a href="../_lib/images/photoshop-challenges/lg/frodo-kahlo-2.jpg" class="fancybox-effects-c" data-fancybox-group="thumb" title="“Frodo Kalho” - Challenge by Nate Geisler">
							<img src="../_lib/images/photoshop-challenges/sm/frodo-kahlo-2.jpg" alt="frodo-kahlo-2.jpg">
						</a>
					</li>
					<li class="columns two">
						<a href="../_lib/images/photoshop-challenges/lg/civil-worf.jpg" class="fancybox-effects-c" data-fancybox-group="thumb" title="“Civil Worf” - Challenged by the existence of the pun">
							<img src="../_lib/images/photoshop-challenges/sm/civil-worf.jpg" alt="civil-worf.jpg">
						</a>
					</li>
					<li class="columns two">
						<a href="../_lib/images/photoshop-challenges/lg/gorilla-notebook.jpg" class="fancybox-effects-c" data-fancybox-group="thumb" title="“Gorilla Notebook” - Challenged by seeing it in there">
							<img src="../_lib/images/photoshop-challenges/sm/gorilla-notebook.jpg" alt="gorilla-notebook.jpg">
						</a>
					</li>
					<li class="columns two">
						<a href="../_lib/images/photoshop-challenges/lg/raising-the-fence-in-winter-iwo-jima.jpg" class="fancybox-effects-c" data-fancybox-group="thumb" title="“Raising the Fence in Winter Iwo Jima” - Challenged by seeing it in there">
							<img src="../_lib/images/photoshop-challenges/sm/raising-the-fence-in-winter-iwo-jima.jpg" alt="raising-the-fence-in-winter-iwo-jima.jpg">
						</a>
					</li>
					<li class="columns two">
						<a href="../_lib/images/photoshop-challenges/lg/rust-kitty.jpg" class="fancybox-effects-c" data-fancybox-group="thumb" title="“Rust Kitty” - Challenged by seeing it in there">
							<img src="../_lib/images/photoshop-challenges/sm/rust-kitty.jpg" alt="rust-kitty.jpg">
						</a>
					</li>
				</ul>
				<? /*
				<ul class="thumbnails-default">
					<?php
						buildThumbnails($project_name);
					?>
				</ul>
				*/ ?>
			</div>
			<div class="row">
				<ul class="thumbnails-default">
					<li class="columns two">
						<a href="../_lib/images/photoshop-challenges/lg/jabba-no-bargain.jpg" class="fancybox-effects-c" data-fancybox-group="thumb" title="“Jabba No Bargain” - Challenged by the “Do Or Do Not... - Dumbledore/Gandalf” meme">
							<img src="../_lib/images/photoshop-challenges/sm/jabba-no-bargain.jpg" alt="jabba-no-bargain.jpg">
						</a>
					</li>
			</div>
		</section>
	</div><!-- /. main -->
<?php include "../inc/footer.php"; ?>
