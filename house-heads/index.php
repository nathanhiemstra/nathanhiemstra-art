<?
	// variables
	$title = "House Heads";
	$section_name = "photographs";
	$project_name = "house-heads";
	$page_name = "house-heads";
	$description = "Since I was a kid I've personified houses as faces. These images connect those faces with human bodies of a corresponding era.";

	include "../inc/header-html.php";
	include "../_lib/functions/build-thumbnails.php";
?>

</head>
<body class="p-<?=$page_name?>">
	<? include "../inc/header.php"; ?>
	<div class="row">
		<header role="banner">
			<div class="container hd clearfix">
				<h1><?=$title?></h1>
			</div><!-- end .container -->

		</header><!-- end #hd -->
	</div>
	<div class="container hd" class="main">
		<section>
			<div class="row">
				<ul class="thumbnails-default">
					<?php
						buildThumbnails($project_name);
					?>
				</ul>
			</div>
		</section>
	</div><!-- /. main -->
<?php include "../inc/footer.php"; ?>
