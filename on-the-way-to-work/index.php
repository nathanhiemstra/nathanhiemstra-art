<?
	// variables
	$title = "On the Way to Work";
	$section_name = "on-the-way-to-work";
	$project_name = "on-the-way-to-work";
	$page_name = "on-the-way-to-work";
	$cur = 1;
	// $description = "I find photos taken moments apart, then animate them.";

	include "../inc/header-html.php";

	$query_on_the_way_to_work_animated = "SELECT * FROM on_the_way_to_work_animated WHERE on_the_way_to_work_animated.active = '1' ORDER BY on_the_way_to_work_animated.order DESC, on_the_way_to_work_animated.id DESC ";

	// PHP 5 antiquated
	// $on_the_way_to_work_animated = mysql_query($query_on_the_way_to_work_animated, $nh_com_db) or die(mysql_error());
	// $row_on_the_way_to_work_animated = mysql_fetch_assoc($on_the_way_to_work_animated);
	// $totalRows_on_the_way_to_work_animated = mysql_num_rows($on_the_way_to_work_animated);

	// PHP 7
	$on_the_way_to_work_animated = mysqli_query($nh_com_db, $query_on_the_way_to_work_animated) or die(mysqli_connect_error());
	$row_on_the_way_to_work_animated = mysqli_fetch_assoc($on_the_way_to_work_animated);
	$totalRows_on_the_way_to_work_animated = mysqli_num_rows($on_the_way_to_work_animated);

	include "../_lib/functions/build-thumbnails.php"; 
?>

</head>
<body class="p-<?=$page_name?>">
	<? include "../inc/header.php"; ?>
	<div class="row">
		<header role="banner">
			<div class="container hd clearfix">
				<h1><?=$title?></h1>	
				<div class="project-description">
				<!-- 	<p>I love old photos. The escapist in me loves fantasizing about these bygone times and people. I can’t imagine wearing their clothes, being in their state of mind, or fitting into their world. </p>
<p>When I look through old photos I search for shots taken moments apart. When I line them up and fade from one to the next, it creates unexpected movement, bringing these static images to life. When I see the movement of their hair in a breeze, or the shift in their bodies, they become real in a way that seems shouldn’t be possible. The fantasy I’m already in envelopes. I feel like I’ve found a secret passage into another world.  </p> -->

				</div>
			</div><!-- end .container -->
		</header><!-- end #hd -->
	</div>
	<div class="container hd" class="main">
		<section>			
			<div class="row">

				<? /*
				<ul class="list">
					<li>
						<a class="various fancybox.ajax" href="/on-the-way-to-work-ajax/index.php">Ajax</a>
					</li>
				</ul>
				*/ ?>

				<ul class="thumbnails-default">

					<?php do { ?>
					<?

						$new_row = '</ul>';
						$new_row .= "\r\n\t\t\t\t\t";
						$new_row .= '</div>';
						$new_row .= "\r\n\t\t\t\t\t";
						$new_row .= '<div class="row">';
						$new_row .= "\r\n\t\t\t\t\t\t";
						$new_row .= '<ul class="thumbnails-default">';
						$new_row .= "\r\n\t\t\t\t\t\t\t";	
						if ($cur == 5) {
							echo $new_row;
							$cur = 1;
						} 
						$cur++;		


					?>
					<li class="columns three thumb-wrapper">
						<a 
							href="/on-the-way-to-work-<?=$row_on_the_way_to_work_animated['filename']?>/" 
							class="animate-fp-thumb animate-fp-thumb-<?=$row_on_the_way_to_work_animated['filename']?>" 
							>
								<img 
								src="/_lib/images/on-the-way-to-work/<?=$row_on_the_way_to_work_animated['filename']?>/thumbnail.jpg" 
								alt="<?=$row_on_the_way_to_work_animated['title']?>"
								> 
						</a>
					</li>
					<?php } while ($row_on_the_way_to_work_animated = mysqli_fetch_assoc($on_the_way_to_work_animated)); ?>
					<li class="columns three thumb-wrapper">
						<a href="/_lib/images/on-the-way-to-work/motorcycle-temp/lg/daily-motorcycle-granville.gif" class="fancybox-effects-c thumbnail" data-fancybox-group="thumb" rel="fancybox-button">
							<img src="/_lib/images/on-the-way-to-work/motorcycle-temp/sm/daily-motorcycle-granville.gif" alt="Motorcycle"> 
						</a>
					</li>


					<li class="columns three thumb-wrapper">
							<a href="/inspirational-streetwise-magazine-vendor/">
								<img src="/_lib/images/home/thumbnails/inspirational-streetwise-magazine-vendor.jpg" alt="Inspirationsal StreetWise Magazine Vendor thumbnail" class="inspirational-streetwise-magazine-vendor-thumbnail">
							</a>
						</li>


				</ul> 
				
			</div>
		</section>
	</div><!-- /. main -->
<?php include "../inc/footer.php"; ?>


