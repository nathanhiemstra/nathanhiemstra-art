<?
	// variables
	$title = "Craigslist: Scanning Job";
	$section_name = "absurdity-with-strangers";
	$project_name = "absurdity-with-strangers";
	$page_name = "craigslist-scanning-job";
	$description = "";

	include "../inc/header-html.php";
	include "../_lib/functions/build-thumbnails.php"; 
?>
<style type="text/css">
/**/
</style>
</head>
<body class="p-<?=$page_name?>">

	<? include "../inc/header.php"; ?>
	<div class="row">
		<header role="banner">
			<div class="container hd clearfix">
				<h1><?=$title?></h1>	
			</div><!-- end .container -->

		</header><!-- end #hd -->
	</div>
	<div class="container hd" class="main">
		<section>			
			<div class="row">
				<ul class="list-no-default email-list">
					<li class="item craigslist-item">
						<div class="item-content">
							<div class="posting" id="pagecontainer">
								<header class="bchead">
									<div class="contents closed">
										<div class="breadbox">
											<span class="crumb"><span class="faux-link">CL</a></span> 
											&gt; 
											<span class="crumb faux-link">chicago</span> &gt; 
											<span class="crumb faux-link">city of chicago</span> &gt; 
											<span class="crumb faux-link">all services offered </span> &gt; 
											<span class="crumb faux-link">small biz ads</span>
										</div>
									</div>
								</header>
								<section class="dateReplyBar">
									<button class="reply_button">reply</button>
								</section>
								<h2 class="postingtitle">
								  <span class="star"></span>
								  Scanning, Files, Organization Exp. (Available PM and Sat and Sun)
								</h2>
								<p class="main-image">
									<img src="/_lib/images/craigslist-scan-job/slide-scanning-sevice.jpg">
								</p>
								<section class="userbody">
									<section id="postingbody">
										I can help you with Scanning and Organizing your files and many office services. <br><br>Located in Chicago and travel.<br><br>
No order is too small. <br><br>Available PM and the Weekend.
								</section>
								<section class="cltags">
									<ul class="blurbs">
										<li> <!-- CLTAG GeographicArea=Lakeview -->Location: CHICAGO</li>
									</ul><!-- END CLTAGS -->
								</section>
								</section>
							</div>
						</div>
					</li>
					
					<li class="item email-item">
						<div class="item-content">
							<div class="email-header clearfix">
								<div class="left">
									<div class="email-from">Nathan Hiemstra</div> 
									<div class="email-to">x &lt;questionable@yahoo.com&gt;</div>
									Scanning, Files, Organization Exp. (Available PM and Sat and Sun)
								</div>
								<div class="email-date right">April 3, 2010 11:06 PM</div> 
							</div>
							<div class="email-body">
								<p>Hi, I'm looking for someone to scan business cards, fliers, and other misc things for art projects of mine. What do you charge?</p>
								<hr>
								<p>Original craigslist post:<br>
								<span class="faux-link">http://chicago.craigslist.org/chc/lgs/34694754210.html</span>
								</p>	
							</div>
						</div>
					</li>
					<li class="item email-item">
						<div class="item-content">
							<div class="email-header clearfix">
								<div class="left">
									<div class="email-from">x &lt;questionable@yahoo.com&gt;</div> 
									<div class="email-to">Nathan Hiemstra</div>
									RE: Scanning, Files, Organization Exp. (Available PM and Sat and Sun)
								</div>
								<div class="email-date right">April 4, 2010 1:28 AM</div> 
							</div>
							<div class="email-body">
								<p>I haveen't decided yet tell me how many hours and where you are located. How much were you p;lanning ot pay?</p>	
							</div>
						</div>
					</li>
					<li class="item email-item">
						<div class="item-content">
							<div class="email-header clearfix">
								<div class="left">
									<div class="email-from">Nathan Hiemstra</div> 
									<div class="email-to">x &lt;questionable@yahoo.com&gt;</div>
									Scanning, Files, Organization Exp. (Available PM and Sat and Sun)
								</div>
								<div class="email-date right">May 5, 2010 3:43 PM</div> 
							</div>
							<div class="email-body">
								<p>I'm going to pay $120/hr and I have six months worth of work. I'm located on Navy Pier. I love you.</p>	
							</div>
						</div>
					</li>
					<li class="item email-item">
						<div class="item-content">
							<div class="email-header clearfix">
								<div class="left">
									<div class="email-from">x &lt;questionable@yahoo.com&gt;</div> 
									<div class="email-to">Nathan Hiemstra</div>
									reply
								</div>
								<div class="email-date right">May 9, 2010 1:59 PM</div> 
							</div>
							<div class="email-body">
								<p>I work downtown Monday thru Friday.
								I also have someone who could help out if necessary.</p>
								<p>But I do not understand what your comment was about I love you  thats a bit unprofessional dont you think.</p>	
							</div>
						</div>
					</li>
					<li class="item email-item">
						<div class="item-content">
							<div class="email-header clearfix">
								<div class="left">
									<div class="email-from">Nathan Hiemstra</div> 
									<div class="email-to">x &lt;questionable@yahoo.com&gt;</div>
									Re: reply
								</div>
								<div class="email-date right">May 10, 2010 11:16 AM</div> 
							</div>
							<div class="email-body">
								What are you saying? 	
							</div>
						</div>
					</li>
					<li class="item email-item">
						<div class="item-content">
							<div class="email-header clearfix">
								<div class="left">
									<div class="email-from">x &lt;questionable@yahoo.com&gt;</div> 
									<div class="email-to">Nathan Hiemstra</div>
									don't know what you mean?
								</div>
								<div class="email-date right">May 10, 2010 6:17 PM</div> 
							</div>
							<div class="email-body">
								<p>you sent me a message have you read what you sent me and please explain it.
it is listed below.</p>	
							</div>
						</div>
					</li>
					<li class="item email-item">
						<div class="item-content">
							<div class="email-header clearfix">
								<div class="left">
									<div class="email-from">Nathan Hiemstra</div> 
									<div class="email-to">x &lt;questionable@yahoo.com&gt;</div>
									Re: don't know what you mean?
								</div>
								<div class="email-date right">May 11, 2010 1:27 PM</div> 
							</div>
							<div class="email-body">
								Excellent. I want to hire you to work for me and I'm hoping to start very soon. Are you saying you can do the work Monday - Friday, or that's when you work and you are not available? Who is this other person you speak of? Can I trust them like I trust you? It's incredible. 	
							</div>
						</div>
					</li>
					<li class="item email-item">
						<div class="item-content">
							<div class="email-header clearfix">
								<div class="left">
									<div class="email-from">x &lt;questionable@yahoo.com&gt;</div> 
									<div class="email-to">Nathan Hiemstra</div>
									Re: don't know what you mean?
								</div>
								<div class="email-date right">May 11, 2010 6:48 PM</div> 
							</div>
							<div class="email-body">
								<p>I work monday thru friday.  Available on the weekends  and maybe in the evenings.</p>
 
								<p>The other person I am referring to is my sister.</p>
								 
								<p>Yes &nbsp; We are trustworthy.</p>
								 
								<p>Who are you ??</p>
								 
								<p>What kind of a business is this and I must be sure that we will be paid and not ripped off.</p>
							</div>
						</div>
					</li>
					<li class="item email-item">
						<div class="item-content">
							<div class="email-header clearfix">
								<div class="left">
									<div class="email-from">Nathan Hiemstra</div> 
									<div class="email-to">x &lt;questionable@yahoo.com&gt;</div>
									Re: don't know what you mean?
								</div>
								<div class="email-date right">May 13, 2010 9:42 AM</div> 
							</div>
							<div class="email-body">
								Glad you asked. I am Nathan Hiemstra, a Navy Pier artist. I have various collections that I want your help scanning. I hope to make a book of these collections. What will you wear to the interview? Your sister, I will trust you to interview her and report back to me. What kind of questions will you ask her? I'll give you a head start on some of the questions I will ask you: <br>
								What is your past experience in scanning? <br>
								Are you familiar with Macs? <br>
								Are you experienced with Photoshop? <br>
								I will not "rip" you off. We will sign a contract. We are brothers. 
							</div>
						</div>
					</li>
					<li class="item email-item">
						<div class="item-content">
							<div class="email-header clearfix">
								<div class="left">
									<div class="email-from">x &lt;questionable@yahoo.com&gt;</div> 
									<div class="email-to">Nathan Hiemstra</div>
									(No Subject)
								</div>
								<div class="email-date right">May 14, 2010 8:39 PM</div> 
							</div>
							<div class="email-body">
								<p>When I go to an interview people do not ask me what I will be wearing.</p>
 
								<p>I am a Business Professional and I wear a Business Suit.</p>
								 
								<p>However your line of questions is offensive and not professional so we are not interested
								 
								anymore.</p>
								 
								<p>Please do not respond to me anymore.</p>
								 
								<p>Thank you.</p>
								 
								<p>Have a good day.</p>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</section>
	</div><!-- /. main -->
<?php include "../inc/footer.php"; ?>
