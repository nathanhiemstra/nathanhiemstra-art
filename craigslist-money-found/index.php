<?
	// variables
	$title = "Craigslist: Money Found";
	$section_name = "absurdity-with-strangers";
	$project_name = "absurdity-with-strangers";
	$page_name = "craigslist-craigslist-money-found";
	$description = "";

	include "../inc/header-html.php";
	include "../_lib/functions/build-thumbnails.php"; 
?>
<style type="text/css">
/**/
</style>
</head>
<body class="p-<?=$page_name?>">

	<? include "../inc/header.php"; ?>
	<div class="row">
		<header role="banner">
			<div class="container hd clearfix">
				<h1><?=$title?></h1>	
			</div><!-- end .container -->

		</header><!-- end #hd -->
	</div>
	<div class="container hd" class="main">
		<section>			
			<div class="row">
				<ul class="list-no-default email-list">
					<li class="item craigslist-item">
						<div class="item-content">
							<div class="posting" id="pagecontainer">
								<header class="bchead">
									<div class="contents closed">
										<div class="breadbox">
											<span class="crumb"><span class="faux-link">CL</a></span> 
											&gt; 
											<span class="crumb faux-link">chicago</span> &gt; 
											<span class="crumb faux-link">city of chicago</span> &gt; 
											<span class="crumb faux-link">community</span> &gt; 
											<span class="crumb faux-link">lost &amp; found</span>
										</div>
									</div>
								</header>
								<section class="dateReplyBar">
									<button class="reply_button">reply</button>
								</section>
								<h2 class="postingtitle">
								  <span class="star"></span>
								  Money found (Lakeview)
								</h2>
								<div class="mapAndAttrs">
									<img src="/_lib/images/craigslist-money-found/craigslist-map.png">
								</div>
								<p class="main-image">
									<img src="/_lib/images/craigslist-money-found/money.jpg">
								</p>
								<section class="userbody">
									<section id="postingbody">
										I found $3 near Roscoe and Ashland. If you lost it, let me know.<br>
										<br>
								</section>
								<section class="cltags">
									<p class="mapaddress">Roscoe at Ashland
										<small>
											(<a href="#">google map</a>)
											(<a href="#">yahoo map</a>)
										</small>
									</p>
									<ul class="blurbs">
										<li> <!-- CLTAG GeographicArea=Lakeview -->Location: Lakeview</li>
										<li>do NOT contact me with unsolicited services or offers</li>
									</ul><!-- END CLTAGS -->
								</section>
								</section>
							</div>
						</div>
					</li>
					<li class="item email-item">
						<div class="item-content">
							<div class="email-header clearfix">
								<div class="left">
									<div class="email-from">alexandria</div> 
									<div class="email-to">Nathan Hiemstra</div>
									Money Found (Lakeview)
								</div>
								<div class="email-date right">August 15, 2013 3:04 AM</div> 
							</div>
							<div class="email-body">
								<p>You really are stupid aren't you??</p>
								<hr>
								<p>Original craigslist post:<br>
								<span class="faux-link">http://chicago.craigslist.org/chc/laf/4304645358.html</span>
								</p>	
							</div>
						</div>
					</li>
					<li class="item email-item">
						<div class="item-content">
							<div class="email-header clearfix">
								<div class="left">
									<div class="email-from">Nathan Hiemstra</div> 
									<div class="email-to">alexandria</div>
									RE: Money Found (Lakeview)
								</div>
								<div class="email-date right">August 15, 2013 2:44 PM</div> 
							</div>
							<div class="email-body">
								Someone must have dropped it on the ground because I found it on the ground. It was right by my house but I know it wasn't me, lol. Do you?
							</div>
						</div>
					</li>
					<li class="item email-item">
						<div class="item-content">
							<div class="email-header clearfix">
								<div class="left">
									<div class="email-from">alexandria</div> 
									<div class="email-to">Nathan Hiemstra</div>
									RE: Money Found (Lakeview)
								</div>
								<div class="email-date right">August 15, 2013 4:53 PM</div> 
							</div>
							<div class="email-body">
								Its three dollars!!!..keep it! No one will care...
							</div>
						</div>
					</li>
					<li class="item email-item">
						<div class="item-content">
							<div class="email-header clearfix">
								<div class="left">
									<div class="email-from">Nathan Hiemstra</div> 
									<div class="email-to">alexandria</div>
									RE: Money Found (Lakeview)
								</div>
								<div class="email-date right">August 15, 2013 4:55 PM</div> 
							</div>
							<div class="email-body">
								What if they ask for it? Can you help me understand?
							</div>
						</div>
					</li>
					<li class="item email-item">
						<div class="item-content">
							<div class="email-header clearfix">
								<div class="left">
									<div class="email-from">alexandria</div> 
									<div class="email-to">Nathan Hiemstra</div>
									RE: Money Found (Lakeview)
								</div>
								<div class="email-date right">August 15, 2013 5:03 PM </div> 
							</div>
							<div class="email-body">
								No one will ask for it...I doubt they even noticed it was missing...<br>
Look at it like this..you have a bag of chips and your drop three...a homeless person sees them on the floor..are you going to ask for them back or let him eat them...
							</div>
						</div>
					</li>
					<li class="item email-item">
						<div class="item-content">
							<div class="email-header clearfix">
								<div class="left">
									<div class="email-from">Nathan Hiemstra</div> 
									<div class="email-to">alexandria</div>
									RE: Money Found (Lakeview)
								</div>
								<div class="email-date right">August 15, 2013 5:12 PM</div> 
							</div>
							<div class="email-body">
								<p>OMG, this money is from a homeless person? Now I feel really bad! I think I know who it is too, she always has lots of blankets on, even in the summer! She sleeps at the bus stop, even in the day!! I dowt she has teh internet though. Should I print out the Craigslist add and show her? </p>

								<p>Homeless must be so hungry to eat chips off the ground, lol. But that's funny.</p>

								<p>Do you believe in God?</p>
							</div>
						</div>
					</li>
					<li class="item email-item">
						<div class="item-content">
							<div class="email-header clearfix">
								<div class="left">
									<div class="email-from">alexandria</div> 
									<div class="email-to">Nathan Hiemstra</div>
									RE: Money Found (Lakeview)
								</div>
								<div class="email-date right">August 15, 2013 5:16 PM </div> 
							</div>
							<div class="email-body">
								It was a metaphor!!! How old are you?!?!!!!
							</div>
						</div>
					</li>
					<li class="item email-item">
						<div class="item-content">
							<div class="email-header clearfix">
								<div class="left">
									<div class="email-from">Nathan Hiemstra</div> 
									<div class="email-to">alexandria</div>
									RE: Money Found (Lakeview)
								</div>
								<div class="email-date right">August 15, 2013 5:18 PM </div> 
							</div>
							<div class="email-body">
								Are you mad at me? I'm 40.
							</div>
						</div>
					</li>
					<li class="item email-item">
						<div class="item-content">
							<div class="email-header clearfix">
								<div class="left">
									<div class="email-from">alexandria</div> 
									<div class="email-to">Nathan Hiemstra</div>
									RE: Money Found (Lakeview)
								</div>
								<div class="email-date right">August 15, 2013 5:20 PM</div> 
							</div>
							<div class="email-body">
								Really?!! And you are acting like a 5 year old?! Spend the money! If you dont feel right doing that then give it to someone homeless..problem solved..
							</div>
						</div>
					</li>
					<li class="item email-item">
						<div class="item-content">
							<div class="email-header clearfix">
								<div class="left">
									<div class="email-from">Nathan Hiemstra</div> 
									<div class="email-to">alexandria</div>
									RE: Money Found (Lakeview)
								</div>
								<div class="email-date right">August 15, 2013 5:25 PM </div> 
							</div>
							<div class="email-body">
								What if they buy alcohol with it? My cousin accidentally drank beer instead and now he has big problems and I don't want that to happen to me. If the homeless makes me buy for them because he's underage I will feel bad like I made them do it. How do I live a good life? 
							</div>
						</div>
					</li>
					<li class="item email-item">
						<div class="item-content">
							<div class="email-header clearfix">
								<div class="left">
									<div class="email-from">alexandria</div> 
									<div class="email-to">Nathan Hiemstra</div>
									RE: Money Found (Lakeview)
								</div>
								<div class="email-date right">August 15, 2013 5:38 PM </div> 
							</div>
							<div class="email-body">
								No one accidentally buys beer and no one can make you buy them beer...its all a matter of choice...you choose the life you live..people just try and help influence your decisions bit in the long run its you who makes the choice...if you choose the wrong one then that's on you..you need to watch the movie dangerous minds...<br>
Just take the money..buy some bread and some lunch meat make sandwiches and give them to homeless people...you are making this way more complicated then what it has to be
							</div>
						</div>
					</li>
					<li class="item email-item">
						<div class="item-content">
							<div class="email-header clearfix">
								<div class="left">
									<div class="email-from">Nathan Hiemstra</div> 
									<div class="email-to">alexandria</div>
									RE: Money Found (Lakeview)
								</div>
								<div class="email-date right">August 15, 2013 7:11 PM</div> 
							</div>
							<div class="email-body">
								I made the homeless some sandwiches:<br>
<span class="faux-link">http://chicago.craigslist.org/chc/laf/4004646398.html</span><br>
I hope they get them before they go bad. If I have to throw them away or eat them then I'll feel bad because I wasted the homeless money.
							</div>
						</div>
					</li>
					<li class="item craigslist-item">
						<div class="item-content">
							<div class="posting" id="pagecontainer">
								<header class="bchead">
									<div class="contents closed">
										<div class="breadbox">
											<span class="crumb"><span class="faux-link">CL</a></span> 
											&gt; 
											<span class="crumb faux-link">chicago</span> &gt; 
											<span class="crumb faux-link">city of chicago</span> &gt; 
											<span class="crumb faux-link">community</span> &gt; 
											<span class="crumb faux-link">lost &amp; found</span>
										</div>
									</div>
								</header>
								<section class="dateReplyBar">
									<button class="reply_button">reply</button>
								</section>
								<h2 class="postingtitle">
								  <span class="star"></span>
								  Sandwiches for homeless (Lakeview)
								</h2>
								<div class="mapAndAttrs">
									<img src="/_lib/images/craigslist-money-found/craigslist-map.png">
								</div>
								<p class="main-image">
									<img src="/_lib/images/craigslist-money-found/craigslist-sandwiches.jpg">
								</p>
								<section class="userbody">
									<section id="postingbody">
										Hi, I found some money and made sandwiches to give to the homeless. If you are homeless and would like a sandwich I will give it to you.

								<br>
								<br>God bless.
								</section>
								<section class="cltags">
									<p class="mapaddress">Roscoe at Ashland
										<small>
											(<a href="#">google map</a>)
											(<a href="#">yahoo map</a>)
										</small>
									</p>
									<ul class="blurbs">
										<li> <!-- CLTAG GeographicArea=Lakeview -->Location: Lakeview</li>
										<li>do NOT contact me with unsolicited services or offers</li>
									</ul><!-- END CLTAGS -->
								</section>
								</section>
							</div>
						</div>
					</li>
					<li class="item email-item">
						<div class="item-content">
							<div class="email-header clearfix">
								<div class="left">
									<div class="email-from">alexandria</div> 
									<div class="email-to">Nathan Hiemstra</div>
									RE: Money Found (Lakeview)
								</div>
								<div class="email-date right">August 15, 2013 7:14 PM</div> 
							</div>
							<div class="email-body">
								Dude..I meant to literally go hand them to them on the street...
							</div>
						</div>
					</li>
					<li class="item email-item">
						<div class="item-content">
							<div class="email-header clearfix">
								<div class="left">
									<div class="email-from">Nathan Hiemstra</div> 
									<div class="email-to">alexandria</div>
									RE: Money Found (Lakeview)
								</div>
								<div class="email-date right">August 15, 2013 7:35 PM</div> 
							</div>
							<div class="email-body">
								Won't this work?
							</div>
						</div>
					</li>
					<li class="item email-item">
						<div class="item-content">
							<div class="email-header clearfix">
								<div class="left">
									<div class="email-from">alexandria</div> 
									<div class="email-to">Nathan Hiemstra</div>
									RE: Money Found (Lakeview)
								</div>
								<div class="email-date right">August 15, 2013 7:36 PM</div> 
							</div>
							<div class="email-body">
								No..don't be stupid man..<br>
								Homeless people don't have internet!
							</div>
						</div>
					</li>
					<li class="item email-item">
						<div class="item-content">
							<div class="email-header clearfix">
								<div class="left">
									<div class="email-from">Nathan Hiemstra</div> 
									<div class="email-to">alexandria</div>
									RE: Money Found (Lakeview)
								</div>
								<div class="email-date right">August 15, 2013 8:03 PM</div> 
							</div>
							<div class="email-body">
								You might be right. No one has answered the ad yet and I've already eaten two of the sandwiches. I feel like a bad person. Also, I bought some beer because I was afraid he'd make me buy him some but I didn't want to add to his problems if he so instead of giving them to him I'm trying to drink them before he answers my ad and now I'm feeling funny. Am I an alcoholic? What should I do? I WISH I NEVER FOUND THAT MONEY!!! Please help!
							</div>
						</div>
					</li>
<? /*
					<li class="item email-item new-group">
						<div class="item-content">
							<div class="email-header clearfix">
								<div class="left">
									<div class="email-from">Leslie</div> 
									<div class="email-to">Nathan Hiemstra</div>
									Sandwiches for homeless (Lakeview)
								</div>
								<div class="email-date right">August 17, 2013 5:18 PM</div> 
							</div>
							<div class="email-body">
								great idea but I don't think the homeless have access to a computer usually.
								<hr>
								<p>Original craigslist post:<br>
								<span class="faux-link">http://chicago.craigslist.org/chc/laf/4004646398.html</span>
								</p>
							</div>
						</div>
					</li>
					<li class="item email-item">
						<div class="item-content">
							<div class="email-header clearfix">
								<div class="left">
									<div class="email-from">Nathan Hiemstra</div> 
									<div class="email-to">Leslie</div>
									Re: Sandwiches for homeless (Lakeview)
								</div>
								<div class="email-date right">August 18, 2013 8:12 PM</div> 
							</div>
							<div class="email-body">
								Dear Smarty Pants, they can use a smart phone.
							</div>
						</div>
					</li>
					<li class="item email-item">
						<div class="item-content">
							<div class="email-header clearfix">
								<div class="left">
									<div class="email-from">Leslie</div> 
									<div class="email-to">Nathan Hiemstra</div>
									Re: Sandwiches for homeless (Lakeview)
								</div>
								<div class="email-date right">August 18, 2013 9:15 PM</div> 
							</div>
							<div class="email-body">
								Dear Smarty Pants Junior..<br>
								I assumed homeless meant that they were considered pretty broke. And phones are a luxury.  Must be the new breed of homeless. 
							</div>
						</div>
					</li>
					<li class="item email-item">
						<div class="item-content">
							<div class="email-header clearfix">
								<div class="left">
									<div class="email-from">Leslie</div> 
									<div class="email-to">Nathan Hiemstra</div>
									Re: Sandwiches for homeless (Lakeview)
								</div>
								<div class="email-date right">August 18, 2013 9:17 PM</div> 
							</div>
							<div class="email-body">
								regardless of the phone situation- feeding the homeless is commendable. thank you.
							</div>
						</div>
					</li>
					*/ ?>
					
				</ul>
			</div>
		</section>
	</div><!-- /. main -->
<?php include "../inc/footer.php"; ?>
