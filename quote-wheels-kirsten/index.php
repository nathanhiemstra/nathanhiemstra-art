<?
	// variables
	$title = "Quote Wheels - Kirsten";
	$section_name = "social-practice";
	$project_name = "quote-wheels";
	$page_name = "quote-wheels-kirsten";
	$description = "";

	include "../inc/header-html.php";
	include "../_lib/functions/build-thumbnails.php"; 
?>
<style type="text/css">
/**/
</style>
</head>
<body class="p-<?=$page_name?> s-quote-wheels">

	<? include "../inc/header.php"; ?>
	<div class="row">
		<header role="banner">
			<div class="container hd clearfix">
				<h1><?=$title?></h1>	
			</div><!-- end .container -->

		</header><!-- end #hd -->
	</div>
	<div class="container hd" class="main">
		<section>			
			<div class="row">
				<div class="columns eight">
					<div class="cutout-container">
						<img src="../_lib/images/quote-wheels/lg/kirsten.jpg" alt="Quote Wheel - Kirsten" class="full-width">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="columns five offset-one">
					<ul class="quote-list">
						<li>
							<span class="qw-date">10/6/1994</span>
							They think they’re so cool because they’re cheerleaders. But they’re not.
						</li>
						<li>
							<span class="qw-date">10/7/1995</span>
							I swear they’re flag illiterate.
							<span class="context">— At her marching band festival</span>
						</li>
						<li>
							<span class="qw-date">8/17/1996</span>
							I was so... didn’t know what to do.
						</li>
						<li>
							<span class="qw-date">8/22/1996</span>
							My last name is a verb.
							<span class="context">- (Hunt)</span>
						</li>
						<li>
							<span class="qw-date">11/11/2000</span>
							Here, have a mind of your own.
							<span class="context">- Describing the way Alison throws her bowling ball</span>
						</li>
						<li>
							<span class="qw-date">3/11/2001</span>
							And most of us have really well grammar.
						</li>
						<li>
							<span class="qw-date">8/28/2001</span>
							A number is a number and if you’re going to fuck around with it... it’s not real.
						   <span class="context"> — About math</span>
						</li>
						<li>
							<span class="qw-date">11/23/2001</span>
							What, do you think I have an automatic memory?
						</li>
						<li>
							<span class="qw-date">1/12/2002</span>
							I’m not a game.
							<span class="context">- Responding to my saying “ding ding” every time she makes herself laugh.</span>
						</li>
						<li>
							<span class="qw-date">5/12/2002</span>
							I wouldn’t try to put one past me. I just passed genetics.
							<span class="context">— Debating with our mom on how I got my lisp.</span>
						</li>
						<li>
							<span class="qw-date">9/25/2002</span>
							’Cause beef doesn’t fuckin’ bounce.
						</li>
						<li>
							<span class="qw-date">11/10/2002</span>
							Your lungs get worse as you get older, but the candles get more.
						</li>
						<li>
							<span class="qw-date">11/10/2002</span>
							I never thought I’d be 23, Jesus.
						</li>
						<li>
							<span class="qw-date">11/30/2002</span>
							Good memories of bad times.
							<span class="context">- About her first college house.</span>
						</li>
						<li>
							<span class="qw-date">12/24/2002</span>
							Kirsten: What are you doing?<br>
							Our mom: Taking my pill.<br>
							Kirsten: In the middle of handling hot bread?
						</li>
						<li>
							<span class="qw-date">12/24/2002</span>
							I haven’t gotten shit-for-in-my-hand.
							<span class="context">— Playing cards on Christmas Eve.</span>
						</li>
						<li>
							<span class="qw-date">11/12/2003</span>
							People at school were like ’when’s your birthday?” and I was like... “whatever”.
						</li>
						<li>
							<span class="qw-date">12/24/2003</span>
							It doesn’t even matter if you plug it in. It remembers!
							<span class="context">— About her breadmaker</span>
						</li>
						<li>
							<span class="qw-date">12/24/2003</span>
							I can’t even do it because my lungs are so healthy.
							<span class="context">— About our mom’s boyfriend’s coarse laugh.</span>
						</li>
					</ul>
				</div>
			</div>
		</section>
	</div><!-- /. main -->
<?php include "../inc/footer.php"; ?>